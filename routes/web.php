<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

Route::get('/','welcomeController@index');

Auth::routes();
Route::get('/home2', 'HomeController@redirecting');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/visitorRegister','registerController2@viewVisitor');
Route::get('/visitationForm','registerController2@visitForm');
Route::get('/guardInterface1','registerController2@guardInterface1');

Route::get('/visitorDetail','visitorController@currentVisitorDetail');
Route::get('/staffInterface1','staffController@staffInterface1');


Route::post('/requestF','visitorController@index');
Route::post('/visitationRequest','visitorController@visitationRequest');
Route::post('/visitationRequest2','visitorController@visitationRequest2');  
Route::post('/groupRegistration','visitorController@groupRegistration');
Route::get('/display','visitorController@display');

Route::post('/create', 'registerController2@create');
Route::post('/visitor','registerController2@register');
Route::post('/groupFinalRegistration/{id}','visitorController@groupFinalRegistration');
Route::post('/groupvisitationFormSubmition/{id}','visitorController@groupvisitationFormSubmition');

Route::post('/departmentComputerList','guardController@departmentComputerList');
Route::post('/departmentChemList','guardController@departmentChemList');
Route::get('/visitorDetail/{id}','guardController@visitorDetail');

Route::post('/notifyExpand/{id}','staffController@notifyExpand');
Route::get('/notifyExpand2/{id}','staffController@notifyExpand2');
Route::get('/approving/{id}','staffController@approve');
Route::get('/groupApproving/{id}','staffController@groupApprove');
Route::get('/internApproving/{id}','staffController@internApprove');
Route::get('/internDisapproving/{id}','staffController@internApprove');

Route::get('/newMessage','guardController@newMessage');

// for registering intern
Route::post('/internRegister','staffController@internRegister');
Route::post('/internFormSubmit','staffController@internFormSubmit');
Route::post('/eventRegister','eventController@eventRegister');
Route::post('/formGenerator/{id}/{nid}','staffController@generate');
Route::post('/trainingFormGenerator/{id}/{nid}','trainingController@generateTraining');
Route::get('/forIntern','staffController@forIntern');
Route::get('/forEvent','eventController@forEvent');
Route::get('/message/{id}','staffController@message');
Route::get('/notifications','staffController@notification');
Route::post('/notification/get','notificationController@get');
Route::post('/notification/read','notificationController@read');
Route::get('/notification','HomeController@notification');

Route::get('/availableEvent','eventController@events');
Route::get('/listInterns','internController@listIntern');
Route::get('/listTraining','trainingController@listTraining');
Route::get('/inGroup','groupController@group');

Route::get('/listUser','adminController@listUser' );
Route::get('/listCollege','adminController@listCollege' );
Route::get('/listDepartment','adminController@listDepartment' );
Route::get('/edit/{id}','adminController@edit' );
Route::patch('/user/{id}','adminController@update' );
Route::delete('/delete/{id}','adminController@destroy' );
Route::get('/generateReport','adminController@generateReport' );
Route::get('/depReg','adminController@registration' );
Route::get('/colReg','adminController@registration2' );
Route::PATCH('/timeSet/{id}','staffController@timeSet' );
Route::get('/staffApproval/{id}','visitorController@staffApproval');
Route::get('/staffGroupRequest','GroupRequestController@staffGroupRequest');
Route::post('/staffGroupRegistration','GroupRequestController@groupRegistration');
Route::PATCH('/timeSett/{id}','GroupRequestController@timeSet' );

Route::get('/training','trainingController@createTraining' );
Route::post('/trainingRegistration','trainingController@store' );
Route::get('/guardRegister','registerController2@visitorRegister' );
Route::post('/requestF2','registerController2@registering' );
Route::get('/registerr','adminController@showRegisterForm' );
Route::get('/addtional','staffController@addtionalMessage' );
Route::get('/sendNotification/{id}','guardController@sendNotification' );
Route::post('/adminRegister','registerController2@registerAll' );
Route::get('/listingTrainers/{id}','trainingController@listingTrainers' );
Route::post('/storeDepartment','registerController2@storeDepartment' );
Route::post('/storeCollege','registerController2@storeCollege' );
Route::get('/groupDetail/{id}/{gid}','groupController@groupDetail' );

Route::get('/editDepartment/{id}','departmentController@edit' );
Route::patch('/department/{id}','departmentController@update' );
Route::delete('/deleteDepartment/{id}','departmentController@destroy' );

Route::get('/editCollege/{id}','collegeController@edit' );
Route::patch('/college/{id}','collegeController@update' );
Route::delete('/deleteCollege/{id}','collegeController@destroy' );
Route::get('/departmentCollege/{id}','collegeController@departmentCollege' );
Route::get('/departmentCollegeList/{id}','collegeController@departmentCollegeList' );
Route::post('/storeDepartment/{id}','collegeController@storeDepartment' );
Route::get('/seeRequest','requestController@seeRequest' );
Route::get('/expand/{id}','visitorPageController@expand' );
Route::DELETE('/deleteIndividual/{id}','visitorPageController@destroy' );
Route::post('/identifyRequest','requestController@identifyRequest' );





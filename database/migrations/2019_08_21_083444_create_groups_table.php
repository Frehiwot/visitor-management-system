<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
    *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('vid') ;
            $table->unsignedInteger('numberOfFemale');
            $table->unsignedInteger('numberOfMale');
            $table->unsignedInteger('smallestAge');
            $table->unsignedInteger('largestAge');
            $table->string('college');
            $table->date('visitationDate');
            $table->text('reasons');
            $table->unsignedInteger('reasons');
            
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupRepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_reps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('groupId')->unsigned(); //have to be group id
            $table->unsignedInteger('visitorId');
            $table->timestamps();

            // $table->foreign('groupId')->references('id')->on('groups')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_reps');
    }
}

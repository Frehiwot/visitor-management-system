<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('vid');
            $table->string('college');
            $table->string('whomToVisit');
            $table->date('visitationDate');
            $table->date('requestDate');
            $table->text('Reason')->nullable();
            $table->string('departmentName');
            $table->string('staffid')->nullable(); //an id of the person thet we want to visit
            $table->timestamp('approvedTime')->nullable();
            $table->Integer('approved')->default(0);
            $table->string('type');
            $table->string('rep')->nullable();
            $table->timestamps();

            


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_forms');
    }
}

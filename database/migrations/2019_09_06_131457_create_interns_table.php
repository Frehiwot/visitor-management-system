<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('NumberOfStudent');
            $table->string('department');
            $table->unsignedInteger('howLong');
            $table->string('type')->default('intern');
            $table->unsignedInteger('approved')->default(0);
            $table->unsignedInteger('count')->default(0);
            $table->datetime('startDate')->nullable();
            $table->string('staffId');// the person who asks for approval
            $table->timestamps();

            $table->foreign('staffId')->references('staffId')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interns');
    }
}

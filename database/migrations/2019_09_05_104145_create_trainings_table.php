<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('college')->nullable();
            $table->string('department');
            $table->unsignedInteger('numberofTrainies');
            $table->unsignedInteger('count')->default(0);
            $table->unsignedInteger('approved')->default(0);
            $table->datetime('startDate');
            $table->datetime('endDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}

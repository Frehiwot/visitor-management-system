<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');
            $table->string('gender');
            $table->string('email')->nullable();
            $table->integer('phone')->unique();
            $table->timestamp('email_verified_at');
            $table->string('password');
            $table->string('type');  // type of visitor
            $table->string('college');
            $table->string('department');
            $table->string('staffId')->unique();
            $table->rememberToken();
            $table->timestamps();
           
            // $table->foreign('college')->references('name')->on('colleges');
            // $table->foreign('department')->references('name')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

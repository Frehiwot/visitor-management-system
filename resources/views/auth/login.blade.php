

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css"  href="bootstrapp/css/bootstrap.min.css">
        <script src="bootstrap/jquery.js"></script>
        <script src="bootstrapp/js/bootstrap.min.js"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                /* font-family: 'Nunito', sans-serif; */
                /* font-weight: 200; */
                height: 100vh;
                margin: 0;
            }

             .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            
            .m-b-md {
                margin-bottom: 30px;
            } 
            header{
                background-color:#227dc7;
                color:white;
                overflow:hidden;
                margin-top:30px;
            }
            #logo{
                float:left;
                margin-left:10px;
                border-radius:10px;
              
            
            }
            header h2{
                margin-top:20px;
            }
            nav{
               
                overflow:hidden;
                
            }
            nav a
            {
                color:white;
                float:right;
                 
                margin-left:10px;
                text-decoration:none;
                font-size:15px;

            }
            #container
            {
              
                width:100%;
                margin-top:50px;
                
                max-width:80%;
                height: 300px;
                /* background-color:lightgrey; */
                /* background:linear-gradient(rgba(255,255,255,0.2),rgba(255,255,255,0.2)),url("images/im.JPG"); */
                 /* background-image: url("images/im.JPG");  */
                
                overflow: hidden;
                
                
    
                /* Background image is centered vertically and horizontally at all times */
                background-position: center center;

                /* Background image doesn't tile */
                background-repeat: no-repeat;

                /* Background image is fixed in the viewport so that it doesn't move when 
                the content's height is greater than the image's height */
                background-attachment: fixed;

                /* This is what makes the background image rescale based
                on the container's size */
                background-size: cover;
                z-index:-2;
            }
            #c2 a{
                
                margin-left:30px;
                width:300px;
            }
            
            #c2{
                width:35%;
                /* position:relative;
                top:100px;
                left:300px; */
                /* box-shadow:3px 3px 7px lightgrey; */
                padding:10px;
                /* border-style: solid;
                border-color: grey; */
                background:color:rgba(255,255,255,0.3);
                margin-top:10px;

                
                
            }
            #service1,#service2,#service3
		    {
                box-shadow: 3px 3px 7px black;
                margin-left:20px;
		   }
           #container2 ,#container3
		  {
			clear:both;
			
			padding:15px;
			margin:10px;
			overflow: hidden;
		   }
		#container3
		{
			margin-top:100px;

		}
        #container11
		{
            margin-top:250px;
            margin-left:50px;

		}

	
        #mainContainer
        {
            text-align:center;
            margin-top:100px;
        }
       
		#mainContainer2
		{
		    margin-top:120px;	
		    margin-right: 40px;
		    margin-left: 40px;
            width:80%;
            margin:auto;
		}
        #mainContainer2 h3{
            text-align:center;
        }
		#container2 h3,#container3 h3
		{
			text-align:center;
			font-size: 20px;
			font-style:bold;
		}
        footer{
            margin-top:30px;
            box-shadow:3px 3px 7px white;
        }
        footer p{
            color:blue;
            text-align:center;
        }
        #c12{
            margin-bottom:30px;
        }
        #main{
            margin-top:20px;
        }
        #con2{
            /* margin-top:30px; */
            margin-top:100px;
        }
        /* #rules{
            margin-top:100px;
        } */
        
        
        </style>
    </head>
    <body>
        <div >
        <header>
            <div>
                <img src="images/AAUU.png" id="logo" width="80" height="70">
                <h2>Addis Ababa Univerisity</h2>
               
            </div>
            <nav >
                <a href="#mainContainer">Services</a> 
                <a href="#mainContainer2">Contact Us</a>   
                <a href="#container11">Visitation Information</a>
                <a href="#container3">About Us</a>
                <a href="#">Home</a>
                
               

            </nav>
        </header>
        <!-- <div class="container" id="container">
            
        </div> -->
        <div class="container" id="container">
    

             <!-- <div  id="c2" class="col-md-4">
                
                <div >
                   
                </div>
                <div  id="c13">
                    <a href="/visitorRegister" class="btn btn-primary">Visitor</a>
                </div>
                <br />
                <div >
                    <a href="/availableEvent"  class="btn btn-primary now"> Available Events</a>
                </div>
                <br />
                <div >
                   
                </div> -->
                <!-- <div  >
                    <a href="/visitorRegister" class="btn btn-primary">Visitor</a>
                </div> -->
                

            <!-- </div> -->
            <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    <a class="btn btn-link" href="/visitorRegister">
                                        I am Visitor
                                    </a>
                                    
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
        
    
</div>

        
            <!-- @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div> -->


        <div id="container11" class="row">
	<div id="rules" class="col-sm-6 col-md-6">
		<h3>Addis Ababa Univerisity Visitation rules</h3>
		<p>  incorporated some of its previous project ideas and resources and came up with SOLVE IT an innovation completion in selected cities of the country; capitalizing on the idea that the involvement of regional cities is crucial if one wants to create a sustainable local tech market. With such spirit, SOLVE IT was proposed to the US embassy in Addis Ababa. The first round of SOLVE IT, as a result, was conducted in collaboration with the US embassy (From November 30, 2017- September 25, 2018).</p>

		<p>The project sparked and created awareness about the impact of pro-poor technologies via direct seminars, media coverage, marketing collateral. And finally, it delivered a grand finale where the top three projects were selected in the national competition by mobilizing the top three projects, from each city, represented by one or two team members, for a week-long boot camp training before the national competition. In the boot camp the participants got the chance of to visit Ethiopian airlines, ENSA and other technology places. In the bootcamp the participants got a chance to meet and show their project to investors at the end they presented their respective pitch for competition judges.
		</p>
	</div>
	<div id="aauimg" class="col-sm-6 col-md-6">
		<img src="images/E.JPG" height="400">
	</div>
</div>

        <div id="mainContainer" >
	<h3>What We Provide</h3>
	<div id="container2" class="row" >
		<div id="service1" class="col-sm-3 col-md-3" >
		 <h3>For colleges</h3>
		 <p>Hard work pays off. Our competitors get both business and technical trainings in order to ameliorate their projects.</p>
		</div>
		<div id="service2" class="col-sm-4 col-md-4" >
		 <h3>For Departments</h3>
		 <p>Hard work pays off. Our competitors get both business and technical trainings in order to ameliorate their projects.</p>
		</div>
		<div id="service3" class="col-sm-4 col-md-4" >
		 <h3>For visitors</h3>
		 <p>Hard work pays off. Our competitors get both business and technical trainings in order to ameliorate their projects.</p>
		</div>
	</div>
</div>




<div id="container3" class="row">
	<div id="about" class="col-sm-6 col-md-6">
		<h3>About us</h3>
		<p>  incorporated some of its previous project ideas and resources and came up with SOLVE IT an innovation completion in selected cities of the country; capitalizing on the idea that the involvement of regional cities is crucial if one wants to create a sustainable local tech market. With such spirit, SOLVE IT was proposed to the US embassy in Addis Ababa. The first round of SOLVE IT, as a result, was conducted in collaboration with the US embassy (From November 30, 2017- September 25, 2018).</p>

		<p>The project sparked and created awareness about the impact of pro-poor technologies via direct seminars, media coverage, marketing collateral. And finally, it delivered a grand finale where the top three projects were selected in the national competition by mobilizing the top three projects, from each city, represented by one or two team members, for a week-long boot camp training before the national competition. In the boot camp the participants got the chance of to visit Ethiopian airlines, ENSA and other technology places. In the bootcamp the participants got a chance to meet and show their project to investors at the end they presented their respective pitch for competition judges.
		</p>
	</div>
	<div id="aboutimg" class="col-sm-6 col-md-6">
		<img src="images/v.JPG" height="400">
	</div>
</div>

<div id="mainContainer2" >
  
	<h3>Get In Touch</h3>
	<div id="container4" class="row">
       <div class="col-sm-1 col-md-1"></div>
		  	
	  <div id="phone" class="col-sm-3 col-md-3">
		  	<img src="images/phone.png">
		 
		</div> 
        <div class="col-sm-1 col-md-1"></div>
		<div id="email" class="col-sm-3 col-md-3">
		  	<img src="images/email.png">
		  	
		</div>
        <div class="col-sm-1 col-md-1"></div>
		<div id="address" class="col-sm-3 col-md-3">
		  	<img src="images/address.png">
		  	
		</div>
    </div>
	  <div id="container5" class="row">
           <div class="col-sm-1 col-md-1"></div>
	       <div id="phone2" class="col-sm-3 col-md-3">
                <h4>Phone</h4>
                <p>0112345678</p>
		  </div> 
          <div class="col-sm-1 col-md-1"></div>
		  <div id="email2" class="col-sm-3 col-md-3">
                <h4>Email</h4>
                <p>PhramcyToSupplier@gmail.com</p>
		  </div>
          <div class="col-sm-1 col-md-1"></div>
		  <div id="address2" class="col-sm-3 col-md-3">
                <h4>Address</h4>
                <p>Bole</P>
		  </div>
          <div class="col-sm-1 col-md-1"></div>
       </div>
    </div>
</div>

<footer>
 <p>2018 right reserved</p>
</footer>
    </body>
</html>


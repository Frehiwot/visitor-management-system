@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                @if($user->type === "visitor")
                    <h1>hello visitor</h1>
                @elseif($user->type === "Staff")
                    <h1>hello staff</h1>
                @endif
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
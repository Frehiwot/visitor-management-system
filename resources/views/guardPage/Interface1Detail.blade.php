@extends('layouts.app')

@section('content')
<div class="container">
    @include('guardLayout.layout')

    

    <div class="card" id="tableCon">
        <div class="card-header">
             <Strong>staff Information</Strong> 
        </div>
        <div class="card-body">
        <strong>Full Name: </strong>
        @foreach($staff as $user)
            {{ $user -> firstname }} {{ $user -> middlename }} {{ $user -> lastname }}
            <br/> <br />
                
            <h6><strong>Department: </strong> {{ $user->department }}</h6> 
            <strong>staffId: </strong> {{$user ->staffId }} <br />
            <button id="messageBtn" class="btn btn-primary">Message</button>
            <div id="messageDiv">
            @if(count($messages)>=1)
                @foreach($messages as $message)
                    Date:-{{ $message -> datee }} <br />
                    Message:-{{ $message -> message }}
                @endforeach
            @else
                 No Message So Far
            @endif
            </div>
            <br />
        @endforeach 
            <table class="table table-hover">

               <th>firstname</th><th>middlename</th><th>lastname</th><th>gender</th><th>country</th><th>city</th><th>Action</th>
               
               @foreach($stafff as $form)
                    @foreach($visitor as $visit)
                        @if($form->vid === $visit->id)
                        <tr>
                            <td>{{ $visit->firstname }} </td>  
                            <td>{{ $visit->middlename }} </td> 
                            <td>{{ $visit->lastname }} </td> 
                            <td>{{ $visit->gender }} </td> 
                            <td>{{ $visit->country }} </td>
                            <td>{{ $visit->city }} </td> 
                            <td> <a href="/visitorDetail/{{ $visit->id }}" name="{{ $visit->id }}" id="btnn" class="btn btn-primary">Detail </a></td>
                           
                        <tr> 
                            
                        @endif
                    @endforeach
                @endforeach
              </table>
</div>
    

    
</div>
@endsection
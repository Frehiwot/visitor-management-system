@extends('layouts.app')

@section('content')
<div class="container">
    @include('guardLayout.layout')

    

    <div class="header"id="staffContainer" >
        
        
        <div class="card">
        <div class="card-header">
                    <Strong>{{ $visitor -> firstname }} {{ $visitor -> middlename }} {{ $visitor -> lastname }}</Strong> 
            </div>
            <div class="card-body">
                <div id="infoContainer">
                    <div id="info">
                        Gender:{{ $visitor-> gender }} <br />
                        Age: {{ $visitor -> age }} <br />
                        country:{{ $visitor->country }} <br />
                        city:{{ $visitor->city }} <br />
                        phone:{{$visitor-> phone }} <br />
                        email:{{ $visitor->email }}<br />
                    </div>
                    <div id="photo">
                    <img src="{{ asset('images/'.$visitor -> photo .'') }} "  width="100" height="100" >
                    </div>
                 </div>
                 <div id="visitationDetail">
                     visitationDate:{{ $form->visitationDate }}<br />
                     requestDate: {{ $form ->requestDate }}<br />
                     Reason:{{ $form->Reason }}
                 </div>
                @if($approved == 0)

                   
                @elseif($approved == 1)
                <div id="approveCon">
                     <strong>Approved By:-{{ $staff->firstname }} </strong>
                 </div>
                    
                 @endif
                    
            </div>  
        </div>
        
    </div>

    
</div>
@endsection
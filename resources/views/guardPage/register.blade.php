@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Visitor Information') }}</div>

                <div class="card-body">
                    <form method="POST" action="/requestF2" enctype="multipart/form-data">
                        @csrf
                       <fieldset>
                           <legend></legend>
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('FULLNAME') }}</label>

                                <div class="col-md-3">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" name="name"  placeholder="firstName" required  >

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <input id="middlename" type="text" class="form-control @error('middlename') is-invalid @enderror" name="middlename" value="{{ old('middlename') }}" placeholder="middleName"required >

                                    @error('middlename')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" placeholder="lastName" required >

                                    @error('lastname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gender" class="col-md-3 col-form-label text-md-right">{{ __('Gender') }}</label>

                                <div class="col-md-3">

                                    <select name="gender" id="" class="form-control">
                                        <option value="female">female</option>
                                        <option value="male">male</option>
                                        
                                    </select>

                                    @error('gender')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                              
                            </div>
                      
                         
                            <div class="form-group row">
                                <label for="vtype" class="col-md-3 col-form-label text-md-right">visitorType</label>
                                <div class="col-md-3">

                                        <select name="vtype" id="vtype" class="form-control">
                                            <option value="individual">individual</option>
                                            <!-- <option value="group">group</option> -->
                                        
                                        </select>

                                        @error('vtype')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                                    <div class="col-md-3">

                                        <input id="Age" type="number" class="form-control @error('Age') is-invalid @enderror" name="Age" value="{{ old('city') }}" required autocomplete="Age" placeholder="Age" autofocus>

                                            @error('Age')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        
                                    
                                </div>


                            

                            <div class="form-group row">
                                <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-3">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"   placeholder="email address" autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required  placeholder="phone number" autocomplete="phone">

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                               
                                
                               
                            </div>

                       <strong>Visitation Information</strong>  
                       <div class="form-group row">
                        <label for="department" class="col-md-4 col-form-label text-md-right">{{ __('Department') }}</label>

                            <div class="col-md-6">

                                <select name="department" id="department" class="form-control">
                                    <option value="none">none</option>
                                @foreach($department as $departments)
                                    <option value="{{ $departments->name }}">{{ $departments->name }}</option>
                                @endforeach
                                </select>

                                @error('department')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>




                        <div class="form-group row">
                            <label for="reasons" class="col-md-4 col-form-label text-md-right">{{ __('whomToVisit') }}</label>

                            <div class="col-md-6">

                                <select name="reasons" id="reasons" class="form-control">
                                    
                                    <option value="Department">Department</option>
                                    <option value="staff">staff member</option>
                                    
                                </select>

                                @error('reasons')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        


                        <div class="form-group row">
                            <label for="reason" class="col-md-4 col-form-label text-md-right">{{ __('Reason') }}</label>

                            <div class="col-md-6">
                                <textarea name="reason" id="reason" cols="30" rows="10" class="form-control" placeholder="enter ur reason here"></textarea>
                                @error('reason')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <strong >If u choose staff member fill the below name otherwise jump it</strong>
                        <div class="form-group row">
                        <label for="staffName" class="col-md-4 col-form-label text-md-right">{{ __('staffName') }}</label>

                            <div class="col-md-6">

                                <select name="staff" id="staff" class="form-control">
                                @foreach($staff as $staffs)
                                    <option value="{{ $staffs->id }}">{{ $staffs->firstname }} {{ $staffs->middlename }} {{ $staffs->lastname }}</option>
                                @endforeach
                                </select>

                                @error('staff')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <!-- <strong>property Information</strong>
                        <div class="form-group row">
                            <label for="propertyType" class="col-md-3 col-form-label text-md-right">{{ __('property') }}</label>

                            <div class="col-md-3">
                                <input id="propertyType" type="text" class="form-control @error('propertyType') is-invalid @enderror" name="propertyType" placeholder="Type" value="{{ old('propertyType') }}" required autocomplete="propertyType" autofocus>

                                @error('propertyName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-3">
                                <input id="propertyName" type="text" class="form-control @error('propertyName') is-invalid @enderror" name="propertyName" placeholder="Name" value="{{ old('propertyName') }}" required autocomplete="propertyName" autofocus>

                                @error('propertyName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-3">
                                <input id="propertyNumber" type="text" class="form-control @error('propertyNumber') is-invalid @enderror" name="propertyNumber" value="{{ old('propertyNumber') }}" placeholder="IdentificationNumber" required autocomplete="propertyNumber" autofocus>

                                @error('propertyName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> -->

                    </fieldset>
                        
                      <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Next') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
@include('guardLayout.layout')

<div id="nowContainer2">
    <div id="search" class="form-group row">
        <div class="col-md-8"></div>
        <input type="search" name="filter" id="filter" placeholder="filter department" class="form-control col-md-4">
     </div>
    @foreach($department as $departments)
        <form method="post" action="/departmentComputerList">
            @csrf
            <div class="card">
                <div class="card-header">
                    <Strong>{{ $departments->name }}</Strong> 
                </div>
                <div class="card-body">
                    College:{{ $college -> name}}.<br />
                    Location:{{ $college -> location}}.<br/> <br />
                    <div class="form-group row" >
                        <select name="staff" placholder="enter staff member name" class="form-control col-md-4 col-sm-4"> 
                            <option value="none">none</option>
                            @foreach($staff as $staffs)
                                @if($staffs->department == $departments->name)
                                    <option value="{{ $staffs->staffId }}">{{ $staffs-> firstname }} {{$staffs->middlename}} {{$staffs->lastname }}</option>
                                 @endif
                            
                            @endforeach
                        </select>
                        <div class="col-md-6"></div>
                        <input type="submit" class="btn btn-primary col-md-2" id="btnExpand" value="Next">
                    </div>
                        
                </div>     
            </div>
        </form>
    @endforeach

</div>
@endsection
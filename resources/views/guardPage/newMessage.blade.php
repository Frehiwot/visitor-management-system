@extends('layouts.app')

@section('content')
<div class="container">
@include('guardLayout.layout')
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">Message Dashboard</div>
                @if(count($latestMessage) >= 1)

                    @foreach($latestMessage as $message)
                        @foreach($staff as $sender)
                            @if($sender->staffId == $message->sid)
                                <div class="card-body">
                                <Strong>Message</strong>
                                Sent By:-{{ $sender->firstname}}<br />
                                Id:-{{ $sender->staffId}}<br />
                                Type:- {{ $message-> messageType }}<br />
                                MessageSentDate:- {{ $message-> datee}}<br />
                                Message:-{{ $message->message }}
                                </div>
                                
                        <!-- check other things also -->
                            @endif
                        @endforeach
                    @endforeach
                @else
                   <strong>There is no new message</strong>
                @endif
               
            </div>
        </div>
    </div>
</div>
@endsection
@component('mail::message')
# VisitorId

<img src="{{ asset('images/'.$visitor -> photo .'') }} "  width="100" height="100" >
<strong>Visitor Information</strong><br />
Full Name:{{ $visitor->firstname}} {{ $visitor->middlename}} {{ $visitor->lastname}}<br />
Id :{{ $visitor->id }} <br />
Age: {{ $visitor->age }} <br />
Email:{{ $visitor->email }} <br />
Phone : {{ $visitor->phone }} <br />
<strong>Visitation Detail</strong><br />
RequestDate:{{ $request-> requestDate }} <br />
VisitationDate: {{ $request -> visitationDate}} <br />
@if($request-> approvedTime !== NULL )
    approvedTime:- {{ $request-> approvedTime }}<br />
@endif
College: {{ $request -> college }}<br />
valid: <strong>FOR ONE DAY VISITATION </strong>

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

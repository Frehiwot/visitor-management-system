@extends('layouts.app')

@section('content')
<div class="container">
    @include('adminLayout.layout')

    

    <div class="header"id="staffContainer" >
        
        
        <div class="card">
        <div class="card-header">
                    <Strong>generateReport</Strong> 
            </div>
            <div class="card-body">
                <div id="infoContainer">
                    <div id="info">
                        TotalNumberOfIndividualVisitor:{{ count($form) }} <br />
                        departmentVisitor: {{ count($formDepartment) }} <br />
                        
                        staffVisitors:{{ count($formStaff) }} <br />
                        approvedVisitaion:{{ count($formApproved) }} <br />
                        visitationApprovedByDepartment:{{ count($formDepartmentApproved) }} <br />
                        visitationApprovedByStaff:{{ count($formStaffApproved) }}<br />
                        disapprovedVisitation:{{count($formDisapproved) }} <br />
                        @foreach($college as $colleges)
                        <h6>{{ $colleges->name }}</h6>
                            @foreach($department as $departments)
                            
                                @foreach($formDepartment as $forms)
                                    @if($forms->departmentName == $departments->name)
                                    <input type="hidden" value="{{$p++}}">
                                    @endif
                                @endforeach
                                <strong> {{ $departments->name }}:-{{ $p }} visitation</strong><br/>
                                 <input type="hidden" value="{{ $p=0 }}">
                                 <div>
                                 <table>
                                 @foreach($user as $users)
                                    @foreach($formStaff as $forms)
                                        @if($users->staffId == $forms->staffid)
                                        @if($users->department == $departments->name)
                                            <input type="hidden" value="{{$p++}}"> 
                                        @endif
                                        
                                        @endif
                                    @endforeach
                                    <tr>
                                    <td><strong > {{ $users->firstname }}</td><td>{{ $p }} visitation</strong><br/></td>
                                    <input type="hidden" value="{{ $p=0 }}">
                                    </tr>
                                 @endforeach
                                 </table>
                                 </div>
                             @endforeach
                        @endforeach
                        
                        
                    </div>
                    
                 </div>
                
                    
            </div>  
        </div>
        
    </div>

    
</div>
@endsection
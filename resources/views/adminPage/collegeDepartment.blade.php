@extends('layouts.app')

@section('content')
<div class="container">
@include('adminLayout.layout')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('DepartmentRegister') }}</div>

                <div class="card-body">
                    <form method="POST" action="/storeDepartment/{{ $college->id }}">
                        @csrf

                        <div class="form-group row">
                            <label for="depName" class="col-md-4 col-form-label text-md-right">{{ __('departmentName') }}</label>

                            <div class="col-md-6">
                                <input id="depName" type="text" class="form-control @error('depName') is-invalid @enderror" name="depName"  required autofocus>

                                @error('depName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rep" class="col-md-4 col-form-label text-md-right">{{ __('represenativeId') }}</label>

                            <div class="col-md-6">
                                <input id="rep" type="text" class="form-control @error('rep') is-invalid @enderror" name="rep" value="{{ old('rep') }}" required autocomplete="rep" autofocus>

                                @error('rep')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
   @include('adminLayout.layout')

<div class="card " id="tableCon">
        <div class="card-header">
             <Strong>Colleges List</Strong> 
        </div>
        <div class="card-body">
       
        @if (session('message'))
            <div class="alert alert-success" role="alert">
                {{ session('message') }}
            </div>
        @endif

            <table class="table table-hover">

               <th>collegeName</th><th>representaive</th><th>university</th><th>location</th><th>Action1</th><th>Action2</th>
               
               @foreach($college as $colleges)
                    
                    <tr>
                        <td>{{ $colleges->name }} </td>  
                        <td>{{ $colleges->representative }} </td> 
                        <td>{{ $colleges->college }} </td> 
                        <td>{{ $colleges->location }} </td> 
                        
                        <td> <a href="/editCollege/{{ $colleges->id }}" name="{{ $colleges->id }}" id="btnn1" class="btn btn-primary">Edit </a> <a href="#" id="git"class="btn btn-primary">Department</a>
                        <br />
                        <br />
                        <div id="depLinks">
                        <a href="/departmentCollege/{{ $colleges->id }}" name="{{ $colleges->id }}" id="btnn1" class="btn btn-primary">registerDepartment </a><br /><br/>
                        <a href="/departmentCollegeList/{{ $colleges->id }}" name="{{ $colleges->id }}" id="btnn1" class="btn btn-primary">listDepartment </a></td>
                        </div>
                        <td> <form method="post" action="/deleteCollege/{{ $colleges->id }}">
                            {{ method_field('DELETE')}}
                            @csrf
                               <input type="submit" class="btn btn-primary" value="Delete">
                            </form> 
                           
                        </td>
                    <tr> 
                            
                      
                @endforeach
              </table>
</div>
    

    
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
@include('adminLayout.layout')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('CollegeRegister') }}</div>

                <div class="card-body">
                    <form method="POST" action="/storeCollege">
                        @csrf

                        <div class="form-group row">
                            <label for="colName" class="col-md-4 col-form-label text-md-right">{{ __('collegeName') }}</label>

                            <div class="col-md-6">
                                <input id="colName" type="text" class="form-control @error('colName') is-invalid @enderror" name="colName"  required  autofocus>

                                @error('colName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rep" class="col-md-4 col-form-label text-md-right">{{ __('represenativeId') }}</label>

                            <div class="col-md-6">
                                <input id="rep" type="text" class="form-control @error('rep') is-invalid @enderror" name="rep" value="{{ old('rep') }}" required autocomplete="rep" autofocus>

                                @error('rep')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Location') }}</label>

                            <div class="col-md-6">
                                <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" required autocomplete="location" autofocus>

                                @error('location')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="parent" class="col-md-4 col-form-label text-md-right">{{ __('Parent') }}</label>

                            <div class="col-md-6">
                                <input id="parent" type="text" class="form-control @error('parent') is-invalid @enderror" name="parent" value="{{ old('parent') }}" required autocomplete="parent" autofocus>

                                @error('location')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="parentName" class="col-md-4 col-form-label text-md-right">{{ __('ParentName') }}</label>

                            <div class="col-md-6">
                                <input id="parentName" type="text" class="form-control @error('parentName') is-invalid @enderror" name="parentName" value="{{ old('parentName') }}" required autocomplete="parentName" autofocus>

                                @error('parentName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
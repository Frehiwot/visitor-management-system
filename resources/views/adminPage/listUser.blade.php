@extends('layouts.app')

@section('content')
<div class="container">
    
<div class="card " id="tableConn">
        <div class="card-header">
             <Strong>staff Information</Strong> 
        </div>
        <div class="card-body">
       
          
            <table class="table table-hover">

               <th>firstname</th><th>middlename</th><th>lastname</th><th>gender</th><th>type</th><th>staffid</th><th>Email</th><th>Phone_Number</th><th>Action</th>
               
               @foreach($user as $visit)
                    
                    <tr>
                        <td>{{ $visit->firstname }} </td>  
                        <td>{{ $visit->middlename }} </td> 
                        <td>{{ $visit->lastname }} </td> 
                        <td>{{ $visit->gender }} </td> 
                        <td>{{ $visit->type }} </td>
                        <td>{{ $visit->staffId }} </td> 
                        <td>{{ $visit->email }} </td>
                        <td>{{ $visit->phone }} </td> 
                        <td> <a href="/edit/{{ $visit->id }}" name="{{ $visit->id }}" id="btnn1" class="btn btn-primary">Edit </a></td>
                        <td> <form method="post" action="/delete/{{ $visit->id }}">
                            {{ method_field('DELETE')}}
                            @csrf
                               <input type="submit" class="btn btn-primary" value="Delete">
                            </form>
                        </td>
                    <tr> 
                            
                      
                @endforeach
              </table>
</div>
    

    
</div>
@endsection
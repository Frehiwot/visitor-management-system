@extends('layouts.app')

@section('content')
<div class="container">
   @include('adminLayout.layout')

<div class="card " id="tableCon">
        <div class="card-header">
             <Strong>Department List</Strong> 
        </div>
        <div class="card-body">
       
          
            <table class="table table-hover">

               <th>departmentName</th><th>representaive</th><th>college</th><th>location</th><th>Action</th>
               
               @foreach($department as $departments)
                    
                    <tr>
                        <td>{{ $departments->name }} </td>  
                        <td>{{ $departments->representative }} </td> 
                        <td>{{ $departments->college }} </td> 
                        <td>{{ $departments->location }} </td> 
                        
                        <td> <a href="/editDepartment/{{ $departments->id }}" name="{{ $departments->id }}" id="btnn1" class="btn btn-primary">Edit </a></td>
                        <td> <form method="post" action="/deleteDepartment/{{ $departments->id }}">
                            {{ method_field('DELETE')}}
                            @csrf
                               <input type="submit" class="btn btn-primary" value="Delete">
                            </form>
                        </td>
                    <tr> 
                            
                      
                @endforeach
              </table>
</div>
    

    
</div>
@endsection
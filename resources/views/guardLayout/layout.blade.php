<div  id="nowContainer">
    <nav >
    <a href="/guardInterface1" class="btn btn-primary now"> Home</a>
    <a href="/guardRegister" class="btn btn-primary now"> RegisterVisitor</a>
    
    <a href="#" class="btn btn-primary now" id="visitorDetail"> listVisitors</a>
    <div id="visitorLinks">
        <a href="/visitorDetail"  class="btn btn-primary now"> individuals</a>
        <a href="/availableEvent" class="btn btn-primary now"> For Event</a>
        <a href="/inGroup" class="btn btn-primary now"> In group</a>
        <a href="/listInterns" class="btn btn-primary now"> For Intern</a>
        <a href="/listTraining" class="btn btn-primary now"> For Training</a>
   </div>
    <!-- real time message -->
    <a href="/newMessage"  class="btn btn-primary now"></span> SeeMessage</a> 
    
    <form id="logout-form" action="{{ route('logout') }}" method="POST" >
        @csrf
        <input type="submit" value="logout" class="btn btn-primary now" id="logoutBtn">
    </form>
                    
    
    <mine-component v-bind:notifications="notifications"></mine-component>
    
    </nav>
</div>
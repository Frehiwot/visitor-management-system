@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <!-- @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif -->

                    You are logged in!
                    @guest
                    
                        @if (Route::has('register'))
                            
                        @endif
                        @else
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                                @csrf
                                <input type="submit" value="logout" class="btn btn-primary now" id="logoutBtn">
                            </form>
                                    
                    @endguest
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

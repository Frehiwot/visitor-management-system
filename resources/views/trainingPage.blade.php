@extends('layouts.app')

@section('content')
<div class="container">
@include('staffLayout.layout')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Training Registration') }}</div>

                <div class="card-body">
                    <form method="POST" action="/trainingRegistration" enctype="multipart/form-data">
                        @csrf
                       
                            <div class="form-group row">
                                <label for="trainName" class="col-md-4 col-form-label text-md-right">{{ __('trainingName') }}</label>

                                <div class="col-md-6">
                                    <input id="trainName" type="text" class="form-control @error('trainName') is-invalid @enderror" value="{{ old('trainName') }}" name="trainName"  required  >

                                    @error('trainName')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="college" class="col-md-4 col-form-label text-md-right">{{ __('College') }}</label>

                                <div class="col-md-6">

                                    <select name="college" id="college" class="form-control">
                                    @foreach($college as $colleges)
                                        <option value="{{ $colleges->name }}">{{ $colleges->name }}</option>
                                    @endforeach
                                    </select>

                                    @error('college')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                            <label for="department" class="col-md-4 col-form-label text-md-right">{{ __('Department') }}</label>

                                <div class="col-md-6">

                                    <select name="department" id="department" class="form-control">
                                         <option value="none">none</option>
                                    @foreach($department as $departments)
                                        <option value="{{ $departments->name }}">{{ $departments->name }}</option>
                                    @endforeach
                                    </select>

                                    @error('department')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                            <label for="datee" class="col-md-4 col-form-label text-md-right">{{ __('TrainingDate') }}</label>

                                <div class="col-md-6">

                                    
                                <div class="checkbox">
                                <label><input type="checkbox" value="MONDAY" name="day1">MONDAY</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="TUSEDAY" name="day2">TUSEDAY</label>
                                </div>
                                <div class="checkbox ">
                                <label><input type="checkbox" value="WEDNSDAY"  name="day3">WEDNSDAY</label>
                                </div>
                                <div class="checkbox">
                                <label><input type="checkbox" value="THURSDAY" name="day4">THURSDAY</label>
                                </div>
                                <div class="checkbox ">
                                <label><input type="checkbox" value="FRIDAY"  name="day5">FRIDAY</label>
                                </div>

                                   
                                </div>
                            </div>
                           
                            
                            <div class="form-group row">
                                <label for="starts" class="col-md-4 col-form-label text-md-right">{{ __('startingDate') }}</label>

                                <div class="col-md-6">
                                <input id="starts" type="datetime-local" class="form-control @error('starts') is-invalid @enderror" name="starts" min="{{$min}}"  value="{{ old('starts') }}" required autocomplete="starts" autofocus>
                                    @error('starts')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                            </div>
                            <div class="form-group row">
                                <label for="ends" class="col-md-4 col-form-label text-md-right">{{ __('endingDate') }}</label>

                                <div class="col-md-6">
                                    <input id="ends" type="datetime-local" class="form-control @error('ends') is-invalid @enderror" value="{{ old('ends') }}" name="ends" min="{{$min}}" placeholder="endTime"  required  >

                                    @error('ends')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                            </div>
                            <div class="form-group row">
                                <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('NumberOfParticipants') }}</label>

                                <div class="col-md-6">
                                    <input id="quantity" type="number" class="form-control @error('quantity') is-invalid @enderror" name="quantity" value="{{ old('quantity') }}" required autocomplete="quantity" autofocus>

                                    @error('quantity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    
                                </div>
                            
                            </div>
                                
                        
                      <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Next') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

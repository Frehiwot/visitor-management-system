@extends('layouts.app')

@section('content')
<div class="container">

<div  id="nowContainer">
    <nav >
    
    <a href="/visitorRegister" class="btn btn-primary now"> RegisterVisitor</a>
    <a href="/visitorDetail"  class="btn btn-primary now"> seeNotifications</a>
    <a href="/newMessage"  class="btn btn-primary now"></span> SeeMessage</a>
    <a href="#"  class="btn btn-primary now"></span> notification</a>
    </nav>
</div>

<div id="nowContainer2">
    <div id="search" class="form-group row">
        <div class="col-md-8"></div>
        <input type="search" name="filter" id="filter" placeholder="filter department" class="form-control col-md-4">
     </div>

</div>


    <div class="header"id="staffContainer" >
        
        
        <div class="card">
        <div class="card-header">
                    <Strong>{{ $visitor -> firstname }} {{ $visitor -> middlename }} {{ $visitor -> lastname }}</Strong> 
            </div>
            <div class="card-body">
                <div id="infoContainer">
                    <div id="info">
                        Gender:{{ $visitor-> gender }} <br />
                        Age: {{ $visitor -> age }} <br />
                        country:{{ $visitor->country }} <br />
                        city:{{ $visitor->city }} <br />
                        phone:{{$visitor-> phone }} <br />
                        email:{{ $visitor->email }}<br />
                    </div>
                    <div id="photo">
                    <img src="{{ asset('images/'.$visitor -> photo .'') }} "  width="100" height="100" >
                    </div>
                 </div>
                 <div id="visitationDetail">
                     visitationDate:{{ $form->visitationDate }}<br />
                     requestDate: {{ $form ->requestDate }}<br />
                     Reason:{{ $form->Reason }}
                 </div>

                 <div id="approveCon">
                     <strong>Approved By:-{{ $staff->firstname }} </strong>
                 </div>
                    
            </div>  
        </div>
        
    </div>
    

@endsection
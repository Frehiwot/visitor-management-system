@extends('layouts.app')

@section('content')
<div class="container">

@include('staffLayout.layout')

<div id="nowContainer2">
    <div id="search" class="form-group row">
        <div class="col-md-8"></div>
        <input type="search" name="filter" id="filter" placeholder="filter no" class="form-control col-md-4">
     </div>

</div>
<div class="header" id="staffContainer" >
   
    @if(count($notify) >= 1)

        @foreach($notify as $notifies)
            <form method="post" action="/notifyExpand/{{ $notifies -> id }}">
            @csrf
                <div class="card">
                <div class="card-header">
                        Notification
                    </div>
                    <div class="card-body">
                        Subject:Visitor Request <br /><br />
                        RequestDate: {{ $notifies->created_at }}
                        <br/> <br />
                        <p>Notification Body:- u have visitor request approval</p>
                         @if($notifies->read_at == NULL)
                            <Strong> U didnot read this notifications</strong>
                         @else
                            <Strong> U have read this notifications</strong>
                        @endif
                        <input type="submit" class="btn btn-primary col-md-2" id="btnExpand" value="Next">
                            
                    </div>  
                </div>
        </form>
        @endforeach
    @elseif(count($notify) == 0)
        <h1>U donot have  notification</h1>
    @endif
    </div>
    

@endsection
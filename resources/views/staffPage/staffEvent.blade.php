@extends('layouts.app')

@section('content')
<div class="container">
@include('staffLayout.layout')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('eventRegister') }}</div>

                <div class="card-body">
                    <form method="POST" action="/eventRegister">
                        @csrf

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('EventType') }}</label>

                            <div class="col-md-6">
                                <input id="type" type="text" class="form-control @error('type') is-invalid @enderror" name="type" value="{{ old('type') }}" required  placeholder="eventType" autofocus>

                                @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                       

                        <div class="form-group row">
                            <label for="eventName" class="col-md-4 col-form-label text-md-right">{{ __('eventName') }}</label>

                            <div class="col-md-6">
                            <input id="eventName" type="text" class="form-control @error('eventName') is-invalid @enderror" name="eventName" value="{{ old('eventName') }}" required  placeholder="eventName" autofocus>

                               

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="department" class="col-md-4 col-form-label text-md-right">{{ __('Department') }}</label>

                                <div class="col-md-6">

                                    <select name="department" id="department" class="form-control">
                                         <option value="none">none</option>
                                    @foreach($department as $departments)
                                        <option value="{{ $departments->name }}">{{ $departments->name }}</option>
                                    @endforeach
                                    </select>

                                    @error('department')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>

                        <!-- make college list -->


                        <div class="form-group row">
                                <label for="datee" class="col-md-4 col-form-label text-md-right">{{ __('eventStarts') }}</label>

                                <div class="col-md-6">
                                    <input id="datee" type="datetime-local" class="form-control @error('datee') is-invalid @enderror" name="datee" value="{{ old('datee') }}" required  autofocus>

                                    @error('datee')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="endDate" class="col-md-4 col-form-label text-md-right">{{ __('eventEnds') }}</label>

                                <div class="col-md-6">
                                    <input id="endDate" type="datetime-local" class="form-control @error('endDate') is-invalid @enderror" name="endDate" value="{{ old('endDate') }}" required  autofocus>

                                    @error('endDate')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>

                        
                       
                        
                        <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('description') }}</label>

                                <div class="col-md-6">
                                    <textarea id="desciption" cols="10" rows="15" name="description" class="form-control" placeholder="description about the event"></textarea>
                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>

                        <div class="form-group row">
                                <label for="allowed" class="col-md-4 col-form-label text-md-right">{{ __('allowed') }}</label>

                                <div class="col-md-6">
                                    <select name="allowed" id="allowed" class="form-control">
                                        <option value="all">all</option>
                                        <option value="registered">registered only</option>
                                        
                                    </select>

                                    @error('allowed')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

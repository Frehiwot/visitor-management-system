@extends('layouts.app')

@section('content')
<div class="container">
@include('staffLayout.layout')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="/internRegister">
                        @csrf

                        <div class="form-group row">
                            <label for="numofStudent" class="col-md-4 col-form-label text-md-right">{{ __('NumberOfStudents') }}</label>

                            <div class="col-md-6">
                                <input id="numofStudent" type="number" class="form-control @error('numofStudent') is-invalid @enderror" name="numofStudent" value="{{ old('numofStudent') }}" required autocomplete="numofStudent" autofocus>

                                @error('numofStudent')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="department" class="col-md-4 col-form-label text-md-right">{{ __('Department') }}</label>

                                <div class="col-md-6">

                                    <select name="department" id="department" class="form-control">
                                         <option value="none">none</option>
                                    @foreach($department as $departments)
                                        <option value="{{ $departments->name }}">{{ $departments->name }}</option>
                                    @endforeach
                                    </select>

                                    @error('department')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>
                        <div class="form-group row">
                            <label for="datee" class="col-md-4 col-form-label text-md-right">{{ __('startDate') }}</label>

                            <div class="col-md-6">
                                <input id="datee" type="date" class="form-control @error('date') is-invalid @enderror" name="datee" min="{{$min}}"  value="{{ old('datee') }}" required autocomplete="datee" autofocus>

                                @error('datee')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dates" class="col-md-4 col-form-label text-md-right">{{ __('ForHowLong') }}</label>

                            <div class="col-md-6">
                                <input id="dates" type="number" class="form-control @error('dates') is-invalid @enderror" name="dates" value="{{ old('dates') }}" required placeholder="In terms of days" autocomplete="dates" autofocus>

                                @error('dates')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

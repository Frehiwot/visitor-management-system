@extends('layouts.app')

@section('content')
<div class="container">

@include('staffLayout.layout')




    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Visitor Information') }}</div>

                <div class="card-body">
                    <form method="POST" action="/addtionMessage" enctype="multipart/form-data">
                        @csrf
                       <fieldset>
                           <legend></legend>
                            <div class="form-group row">
                                <label for="subject" class="col-md-3 col-form-label text-md-right">{{ __('Subject') }}</label>

                                <div class="col-md-6">
                                    <input id="subject" type="text" class="form-control @error('subject') is-invalid @enderror" value="{{ old('subject') }}" name="subject"  placeholder="subject" required  >

                                    @error('subject')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                
                             
                            </div>

                            <div class="form-group row">
                                <label for="message" class="col-md-3 col-form-label text-md-right">{{ __('Message') }}</label>

                                
                                <div class="col-md-6">
                                    <textarea name="message" id="message" cols="30" rows="10">put ur message here</textarea>

                                    @error('message')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                             
                            </div>
                           
                      
                         
                     </fieldset>
                        
                      <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    

@endsection
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css"  href="bootstrapp/css/bootstrap.min.css"> -->
    <script src="{{  asset('bootstrap/jquery.js')}}"></script>
    <!-- //<script src="bootstrapp/js/bootstrap.min.js"></script> -->
    
    <style>
        
        #groupCard{
            margin-left:300px;
                    
            }
        #bothContainer{
            
            width:100%;
        }
        #infoContainer2{
            width:100%;
        }
        #repPhoto{
            margin-left:300px;
            margin-bottom:50px;
        }
    </style>
    
    <script>
        $(document).ready(function(){
			$("#registerLinks").hide();
            $("#messageLinks").hide();
		  $("#mainRegister").click(function(){
		    $("#registerLinks").slideToggle();
          });
          $("#mainMessage").click(function(){
		    $("#messageLinks").slideToggle();
          });

        });
    </script>
   
    
</head>
<body>
    <div >
       
        <header>
            <div>
                <img src="{{ asset('images/AAUU.png')}}" id="logo" width="80" height="70">
                <h2>Addis Ababa Univerisity</h2>
               
            </div>
            <nav id="appNav">
                <a href="/#mainContainer">Servis</a> 
                <a href="/#mainContainer2">Contact Us</a>   
                <a href="/#container11">Visitation Information</a>
                <a href="/#container3">About Us</a>
                <a href="/">Home</a>
               
                
               

            </nav>
           
 </header>

        <main class="py-4">
        <div class="container" id="bothContainer">
    @include('staffLayout.layout')
    <div class="card" id="groupCard">
        <div class="card-header">
                    <Strong>{{ $rep -> firstname }} {{ $rep -> middlename }} {{ $rep -> lastname }}</Strong> 
            </div>
            <div class="card-body">
                <div id="infoContainer2">
                @if($form-> rep ==  "visitor")
                    <div id="repPhoto">
                            <img src="images/{{ $rep -> photo }}" width="100" height="100" >
                    </div>
                @endif
                    
                    
                 </div>
                 <div id="allContainer" class="row">
                 
                    <div id="info" class="col-sm-4 col-md-4">
                    <strong>Group representative Info</strong>
                    @if($form-> rep ==  "visitor")
                            Gender:{{ $rep-> gender }} <br />
                            Age: {{ $rep -> age }} <br />
                            country:{{ $rep->country }} <br />
                            city:{{ $rep->city }} <br />
                            phone:{{$rep-> phone }} <br />
                            email:{{ $rep->email }}<br />
                    @elseif($form->rep =="staff")
                    <br/>
                         sent by: Staff member
                    @endif
                    </div>
                

                 <div id="visitationDetail" class="col-sm-4 col-md-4">
                 <strong>Group Visitation Info</strong>
                     visitationDate:{{ $form->visitationDate }}<br />
                     requestDate: {{ $form ->requestDate }}<br />
                     College:{{ $form->college}} <br />
                     Reason:{{ $form->Reason }}<br />
                     NumberOfFemale:- {{ $group-> NumberOfFemale}} <br />
                     NumberOfMale:- {{ $group-> NumberOfMale}} <br />
                    smallestAge:- {{ $group -> smallestAge }} <br />
                    largestAge:- {{ $group -> largestAge }} <br />   
                 </div>

                 <div id="visitationDetail" class="col-sm-4 col-md-4">
                 <strong>Group organization Info</strong>
                     organizationType:{{ $org ->organizationType }}<br />
                     OrganizationName: {{$org ->organizationName }}<br />
                     organizationCountry:{{ $org->organizationCountry}} <br />
                     organizationCity:{{ $org->organizationCity }}<br />
                     organizationAddress:- {{ $org-> organizationAddress}} <br />
                     
                     
                 </div>
                 
                 </div>
                 @if($form->approvedTime == NULL)
                        <button class="btn btn-primary" id="time">SET Time</button>
                        <br />
                        <div id="timeCon">
                            <form method="POST" action="/timeSett/{{ $form->id }}">
                            {{ method_field('PATCH')}}
                             @csrf
                                <input type="datetime-local" name="setTime">
                                <input type="submit" value="submit">
                            </form>
                        </div>
                    @else
                        approvedTime:-{{ $form -> approvedTime }}
                     @endif
                @if($approved == 0)

                    <div id="approveCon" >
                        <a href="/groupApproving/{{ $form->id }}" id><button class="btn btn-primary">Approve</button></a>
                        <a href="/groupDisapproving/{{ $form->id }}"><button class="btn btn-primary">DisApprove</button></a>
                    </div>
                @elseif($approved >= 1)
                <div id="approveCon" class="col-sm-3 col-md-3">
                     <strong>Approved By:-{{ $staff->firstname }} </strong>
                 </div>
                    
                 @endif
                
                 <br /><br />
                <table class="table table-hover">

                    <th>firstname</th><th>middlename</th><th>lastname</th><th>gender</th><th>country</th><th>city</th>

                    @foreach($groupRep as $groups)
                        @foreach($visitor as $visit)
                            @if($groups->visitorId == $visit->id)
                            <tr>
                                <td>{{ $visit->firstname }} </td>  
                                <td>{{ $visit->middlename }} </td> 
                                <td>{{ $visit->lastname }} </td> 
                                <td>{{ $visit->gender }} </td> 
                                <td>{{ $visit->phone }} </td>
                                <td>{{ $visit->email }} </td> 
                                
                                
                            <tr> 
                                
                            @endif
                        @endforeach
                    @endforeach
                </table>
                    
            </div>  
        </div>
        
    </div>
    
   
</body>
</html>



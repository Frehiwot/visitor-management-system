<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css"  href="bootstrapp/css/bootstrap.min.css"> -->
    <script src="{{  asset('bootstrap/jquery.js')}}"></script>
    <!-- //<script src="bootstrapp/js/bootstrap.min.js"></script> -->
    
    <style>
        
        #internCard{
            margin-left:300px;
                    
            }
        #bothContainer{
            
            width:100%;
        }
    </style>
    
    <script>
        $(document).ready(function(){
			$("#registerLinks").hide();
            $("#messageLinks").hide();
		  $("#mainRegister").click(function(){
		    $("#registerLinks").slideToggle();
          });
          $("#mainMessage").click(function(){
		    $("#messageLinks").slideToggle();
          });

        });
    </script>
   
    
</head>
<body>
    <div >
       
        <header>
            <div>
                <img src="{{ asset('images/AAUU.png')}}" id="logo" width="80" height="70">
                <h2>Addis Ababa Univerisity</h2>
               
            </div>
            <nav id="appNav">
                <a href="/#mainContainer">Servis</a> 
                <a href="/#mainContainer2">Contact Us</a>   
                <a href="/#container11">Visitation Information</a>
                <a href="/#container3">About Us</a>
                <a href="/">Home</a>
               
                
               

            </nav>
           
 </header>

        <main class="py-4">
        <div class="container" id="bothContainer">
    @include('staffLayout.layout')

    
    <div class="card" id="internCard">
        <div class="card-header">
                  <Strong>listOfCreatedRequests</Strong> 
            </div>
            <div class="card-body">
                
                 
               
              @if(count($individual)>1)
                 <table class="table table-hover">

               <th>requestType</th><th>visitorId</th><th>visitationDate</th><th>requestDate</th><th>approved</th><th>Acton1</th><th>Action2</th>
               
               @foreach($individual as $visit)
                   
                        <tr>
                            <td>{{ $visit->type }} </td>  
                            <td>{{ $visit->vid }} </td> 
                            <td>{{ $visit->visitationDate }} </td> 
                            <td>{{ $visit->requestDate }} </td> 
                            <td>{{ $visit->approved }} </td>
                            <td><a href="/editIndividual/{{ $visit->vid }}" name="{{ $visit->vid }}" id="btnn1" class="btn btn-primary">Edit 
                                </a>  <br/><form method="post" action="/deleteIndividual/{{ $visit->id }}" id="deleteForm">
                                {{ method_field('DELETE')}}
                                @csrf
                                <input type="submit" class="btn btn-primary" value="Delete">
                                </form>
                            </td>

                            <td><a href="/expand/{{ $visit->vid }}" name="{{ $visit->id }}" id="btnn1" class="btn btn-primary">Expand </a></td>
                           
                        <tr> 
                            
                        
                @endforeach

                
              </table>
            @endif
              @if(count($training) > 1)
                <table class="table table-hover">
                    <th>requestType</th><th>TrainingName</th><th>startDate</th><th>endDate</th><th>approved</th><th>Acton1</th><th>Action2</th>
                    
                    @foreach($training as $visit)
                        
                                <tr>
                                    <td>{{ $visit->type }} </td>  
                                    <td>{{ $visit->name }} </td> 
                                    <td>{{ $visit->startDate }} </td> 
                                    <td>{{ $visit->endDate }} </td> 
                                    <td>{{ $visit->approved }} </td>
                                    <td><a href="" name="" id="btnn1" class="btn btn-primary">Edit </a> <a href="#" id="git"class="btn btn-primary">Delete</a> </td> 

                                    <td><a href="/listingTrainers" name="" id="btnn1" class="btn btn-primary">Expand </a><a href="/listingTrainers" name="" id="btnn1" class="btn btn-primary">AddParticipants </a></td>
                                
                                <tr> 
                                    
                                
                        @endforeach
                </table>
            @endif

            @if(count($intern)>1)
              <table class="table table-hover">
                <th>requestType</th><th>startDate</th><th>endDate</th><th>approved</th><th>Acton1</th><th>Action2</th>
                
                @foreach($intern as $visit)
                    
                            <tr>
                                <td>{{ $visit->type }} </td>  
                                
                                <td>{{ $visit->startDate }} </td> 
                                <td>{{ $visit->howLong }} </td> 
                                <td>{{ $visit->approved }} </td>
                                <td><a href="" name="" id="btnn1" class="btn btn-primary">Edit </a> <a href="#" id="git"class="btn btn-primary">Delete</a> </td> 

                                <td><a href="" name="" id="btnn1" class="btn btn-primary">Expand </a></td>
                            
                            <tr> 
                                
                            
                    @endforeach
              </table>
            @endif
                    
            </div>  
        </div>
        
    </div>
 
        </main>
    </div>
</body>
</html>
@extends('layouts.app')

@section('content')
<div class="container">

<div  id="nowContainer">
    <nav >
    <a href="/home2" class="btn btn-primary now" > Home</a>
    <a href="#" class="btn btn-primary now" id="mainRegister"> CreateRequest</a>
    <div id="registerLinks">
        <a href="/forIntern" class="btn btn-primary now"> For Intern</a>
        <a href="" class="btn btn-primary now"> For Training</a>
        <a href="/visitorRegister" class="btn btn-primary now"> Individual</a>
        <a href="/staffGroupRequest" class="btn btn-primary now"> Group</a>
   </div>
    <a href="/notifications"  class="btn btn-primary now"> seeNotifications</a>
    <a href="#"  class="btn btn-primary now" id="mainMessage"></span> putMessage</a>
    <div id="messageLinks">
        <a href="/message/1" class="btn btn-primary now"> I am available</a>
        <a href="/message/2" class="btn btn-primary now"> I am not available</a>
        <a href="/addtional" class="btn btn-primary now"> addtional information</a>
   </div>
    @guest
                    
        @if (Route::has('register'))
            
        @endif
        @else
            <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                @csrf
                <input type="submit" value="logout" class="btn btn-primary now" id="logoutBtn">
            </form>
                    
    @endguest
    
            <mine-component v-bind:notifications="notifications"></mine-component>
        
    </li>
  
    </nav>
</div>

<div id="nowContainer2">
    

</div>


    <div class="header"id="staffContainer" >
        
        
        <div class="card">
        <div class="card-header">
                    <Strong>{{ $visitor -> firstname }} {{ $visitor -> middlename }} {{ $visitor -> lastname }}</Strong> 
            </div>
            <div class="card-body">
                <div id="infoContainer">
                    <div id="info">
                        Gender:{{ $visitor-> gender }} <br />
                        Age: {{ $visitor -> age }} <br />
                        country:{{ $visitor->country }} <br />
                        city:{{ $visitor->city }} <br />
                        phone:{{$visitor-> phone }} <br />
                        email:{{ $visitor->email }}<br />
                    </div>
                    <div id="photo">
                        <img src="{{ asset('images/'.$visitor -> photo .'') }} "  width="100" height="100" >
                    </div>
                 </div>
                 <div id="visitationDetail">
                     visitationDate:{{ $form->visitationDate }}<br />
                     requestDate: {{ $form ->requestDate }}<br />
                     Reason:{{ $form->Reason }}
                     <br />
                     <br />
                     <!-- && $type !== 'notify' && $type !== 'individualEmergency' && $type !== 'individual' -->
                     @if($form->approvedTime == NULL ) 
                        <button class="btn btn-primary" id="time">SET Time</button>
                        <br />
                        <div id="timeCon">
                            <form method="POST" action="/timeSet/{{ $form->id }}">
                            {{ method_field('PATCH')}}
                             @csrf
                                <input type="datetime-local" name="setTime">
                                <input type="submit" value="submit">
                            </form>
                        </div>
                    @else
                        approvedTime:-{{ $form -> approvedTime }}
                     @endif
                 </div>
                @if($approved == 0)

                    <div id="approveCon">
                        <a href="/approving/{{ $form->id }}" id><button class="btn btn-primary">Approve</button></a>
                        <a href="/disapproving/{{ $form->id }}"><button class="btn btn-primary">DisApprove</button></a>
                    </div>
                @elseif($approved == 1)
                <div id="approveCon">
                     <strong>Approved By:-{{ $staff->firstname }} </strong>
                 </div>
                    
                 @endif
                    
            </div>  
        </div>
        
    </div>
    

@endsection
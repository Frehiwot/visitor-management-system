@extends('layouts.app')

@section('content')
<div class="container">

@include('staffLayout.layout')

<div id="nowContainer2">
    <div id="search" class="form-group row">
        <div class="col-md-8"></div>
        <input type="search" name="filter" id="filter" placeholder="filter department" class="form-control col-md-4">
     </div>

</div>
<div class="header" id="staffContainer" >
    @foreach($notify as $notifies)
        <form method="post" action="/notifyExpand/{{ $notifies -> id }}">
        @csrf
            <div class="card">
            <div class="card-header">
                    <Strong>Chemistry</Strong> 
                </div>
                <div class="card-body">
                    Subject:Visitor Request <br /><br />
                    RequestDate:
                    <br/> <br />
                    <p>Notification Body:- {{ $notifies->notification }}</p>
                    <input type="submit" class="btn btn-primary col-md-2" id="btnExpand" value="Next">
                        
                </div>  
            </div>
      </form>
    @endforeach
    </div>
    

@endsection
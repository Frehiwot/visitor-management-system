<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css"  href="bootstrapp/css/bootstrap.min.css"> -->
    <script src="{{  asset('bootstrap/jquery.js')}}"></script>
    <!-- //<script src="bootstrapp/js/bootstrap.min.js"></script> -->
    
    <style>
        
        #internCard{
            margin-left:300px;
                    
            }
        #bothContainer{
            
            width:100%;
        }
    </style>
    
    <script>
        $(document).ready(function(){
			$("#registerLinks").hide();
            $("#messageLinks").hide();
		  $("#mainRegister").click(function(){
		    $("#registerLinks").slideToggle();
          });
          $("#mainMessage").click(function(){
		    $("#messageLinks").slideToggle();
          });

        });
    </script>
   
    
</head>
<body>
    <div >
       
        <header>
            <div>
                <img src="{{ asset('images/AAUU.png')}}" id="logo" width="80" height="70">
                <h2>Addis Ababa Univerisity</h2>
               
            </div>
            <nav id="appNav">
                <a href="/#mainContainer">Servis</a> 
                <a href="/#mainContainer2">Contact Us</a>   
                <a href="/#container11">Visitation Information</a>
                <a href="/#container3">About Us</a>
                <a href="/">Home</a>
               
                
               

            </nav>
           
 </header>

        <main class="py-4">
        <div class="container" id="bothContainer">
    @include('staffLayout.layout')

    
    <div class="card" id="internCard">
        <div class="card-header">
                 Sent By:-   <Strong>{{ $staff -> firstname }} {{ $staff -> middlename }} {{ $staff -> lastname }} From {{$staff->department}}</Strong> 
            </div>
            <div class="card-body">
                <div id="infoContainer">
                    <div id="info">
                        NumberOfStudents:- {{ $intern-> NumberOfStudent}} <br />
                        For Howlong:- {{ $intern -> howLong }} days<br />
                        approvedBy:- {{ $intern->staffId }} <br />
                       
                    </div>
                    
                </div>
                 
                @if($intern->approved == 0)

                    <div id="approveCon">
                        <a href="/internApproving/{{ $intern->id }}" id><button class="btn btn-primary">Approve</button></a>
                        <a href="/internDisapproving/{{ $intern->id }}"><button class="btn btn-primary">DisApprove</button></a>
                    </div>
                @elseif($intern->approved == 1)
                    
                    <div id="approveCon">
                        <strong>Approved By:-{{ $rep->firstname }} </strong>
                    </div>
                    @if($intern->NumberOfStudent >3)
                    <div id="approveCon">
                            <a href="/internApproving/{{ $intern->id }}" id><button class="btn btn-primary">Approve</button></a>
                            <a href="/internDisapproving/{{ $intern->id }}"><button class="btn btn-primary">DisApprove</button></a>
                        </div>
                    @endif
                 @elseif($intern->approved > 1)

                   @foreach($approve as $approves)
                     @foreach($user as $users)
                        @if($users->id == $approves->approvedby)
                            <div id="approveCon">
                                <strong>Approved By:-{{ $users->firstname }} </strong>
                            </div>
                        @endif
                    @endforeach
                   @endforeach
                    
                 @endif

                 <table class="table table-hover">

               <th>firstname</th><th>middlename</th><th>lastname</th><th>gender</th><th>country</th><th>city</th>
               
               @foreach($internship as $interns)
                    @foreach($visitor as $visit)
                        @if($interns->vid === $visit->id)
                        <tr>
                            <td>{{ $visit->firstname }} </td>  
                            <td>{{ $visit->middlename }} </td> 
                            <td>{{ $visit->lastname }} </td> 
                            <td>{{ $visit->gender }} </td> 
                            <td>{{ $visit->phone }} </td>
                            <td>{{ $visit->email }} </td> 
                           
                           
                        <tr> 
                            
                        @endif
                    @endforeach
                @endforeach
              </table>
                    
            </div>  
        </div>
        
    </div>
 
        </main>
    </div>
</body>
</html>




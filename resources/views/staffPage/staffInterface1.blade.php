@extends('layouts.app')

@section('content')
<div class="container">
@if( $rep == 0)
    @include('staffLayout.layout')
@else
    @include('staffLayout.repLayout')
@endif

<div id="nowContainer2">
    <div id="search" class="form-group row">
        <div class="col-md-8"></div>
        <input type="search" name="filter" id="filter" placeholder="filter department" class="form-control col-md-4">
     </div>

</div>
<div class="header" id="staffContainer" >
   
    @if(count($notify) >= 1)

        @foreach($notify as $notifies)
            <form method="post" action="/notifyExpand/{{ $notifies -> id }}">
            @csrf
                <div class="card">
                <div class="card-header">
                        Notification
                    </div>
                    <div class="card-body">
                        Subject:Visitor Request <br /><br />
                        RequestDate:
                        <br/> <br />
                        @if($notifies->data['type'] == 'intern')
                        <p>Notification Body:- intern approval request</p>
                        @elseif($notifies->data['type'] == 'individual')
                        <p>Notification Body:- visitor approval request</p>
                        @elseif($notifies->data['type'] == 'group')
                        <p>Notification Body:- group approval request</p>
                        @elseif($notifies->data['type'] == 'notify')
                        <p class="btn btn-primary">Notification Body:- ur visitor has arrived</p>
                        
                        @elseif($notifies->data['type'] == 'individualEmergency')
                        <p class="btn btn-warning">Notification Body:- visitor is waiting ur approval immidiatley</p>
                        @elseif($notifies->data['type'] == 'training')
                        <p class="btn btn-warning">Notification Body:- tarining request approval</p>

                        @endif
                        <input type="submit" class="btn btn-primary col-md-2" id="btnExpand" value="Next">
                            
                    </div>  
                </div>
        </form>
        @endforeach
    @elseif(count($notify) == 0)
        <h1 id="hi">U donot have new notification</h1>
    @endif
    </div>
    

@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Group Information') }}</div>

                <div class="card-body">
                    <form method="POST" action="/groupRegistration" enctype="multipart/form-data">
                        @csrf
                       <fieldset>
                       
                       <input type="hidden" name="property" value="{{ $property }}">
                       @if($property === "yes")
                    
                        <strong>Group Property Information</strong>
                            
                           <div class="form-group row">
                                <label for="propertyType" class="col-md-4 col-form-label text-md-right">{{ __('property') }}</label>
                                 <!-- make it select option -->
                                <div class="col-md-3">
                                    <input id="propertyType" type="text" class="form-control @error('propertyType') is-invalid @enderror" name="Type" value="{{ old('propertyType') }}" required placeholder="type" autofocus>

                                    @error('propertyType')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <input id="propertyName" type="text" class="form-control @error('propertyName') is-invalid @enderror" name="propertyName" value="{{ old('propertyName') }}" required placeholder="Name" autofocus>

                                    @error('propertyName')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                           <div class="form-group row">
                                <label for="propertyNumber" class="col-md-4 col-form-label text-md-right">property</label>

                                <div class="col-md-3">
                                    <input id="propertyNumber" type="text" class="form-control @error('propertyNumber') is-invalid @enderror" name="propertyNumber" value="{{ old('propertyNumber') }}" required placeholder="identificationNumber" autofocus>

                                    @error('propertyName')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <input id="quantity" type="number" class="form-control @error('quantity') is-invalid @enderror" name="quantity" value="{{ old('quantity') }}" placeholder="howMany" required  autofocus>

                                    @error('quantity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>

                            @endif

                       <strong>Group Number Information</strong>
                       <!-- Rep Id is needed -->
                       <input type="hidden" value="{{ $vid }}" name="vid">
                        <div class="form-group row">
                            <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('NumberOf') }}</label>

                            <div class="col-md-3">
                                <input id="quantity" type="number" class="form-control @error('quantity') is-invalid @enderror" name="quantity" value="{{ old('quantity') }}" placeholder="Male" required autocomplete="quantity" autofocus>

                                @error('quantity')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-3">
                                <input id="quantityF" type="number" class="form-control @error('quantityF') is-invalid @enderror" name="quantityF" value="{{ old('quantityF') }}" placeholder="Female" required autocomplete="quantityF" autofocus>

                                @error('quantityF')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="ageSmallestLimit" class="col-md-4 col-form-label text-md-right">{{ __('SmalestAge') }}</label>

                                <div class="col-md-6">

                                <input id="ageSmallestLimit" type="number" class="form-control @error('ageSmallestLimit') is-invalid @enderror" name="ageSmallestLimit" value="{{ old('ageSmallestLimit') }}" required  >

                                    @error('ageSmallestLimit')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ageLargestLimit" class="col-md-4 col-form-label text-md-right">{{ __('LargetsAge') }}</label>

                                <div class="col-md-3">

                                <input id="ageLargestLimit" type="number" class="form-control @error('ageLargestLimit') is-invalid @enderror" name="ageLargestLimit" value="{{ old('ageLargestLimit') }}" required  >

                                    @error('ageLargestLimit')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-3">

                                <input id="howMany" type="number" class="form-control @error('howMany') is-invalid @enderror" name="howMany" value="{{ old('howMany') }}" required  placeholder="how many of u">

                                    @error('howMany')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                        
                        <strong>Group Organization Information</strong>
                        <div class="form-group row">
                                <label for="orgType" class="col-md-4 col-form-label text-md-right">{{ __('organization') }}</label>

                                <div class="col-md-3">
                                    <select name="orgType" id="college" class="form-control" >
                                        <option >----Type----</option>
                                        <option value="school">school</option>
                                        <option value="company">company</option>
                                        
                                    </select>  
                                    @error('orgType')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">

                                <input id="orgName" type="text" class="form-control @error('orgName') is-invalid @enderror" name="orgName" value="{{ old('orgName') }}" required placeholder="Name" autofocus>

                                    @error('orgName')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        
                           

                            <div class="form-group row">
                                <label for="orgCountry" class="col-md-4 col-form-label text-md-right">{{ __('organization') }}</label>

                                <div class="col-md-3">

                                <input id="orgCountry" type="text" class="form-control @error('orgCountry') is-invalid @enderror" name="orgCountry" value="{{ old('orgCountry') }}" required  placeholder="Country" autofocus>

                                    @error('orgCountry')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-3">

                                <input id="orgCity" type="text" class="form-control @error('orgCity') is-invalid @enderror" name="orgCity" value="{{ old('orgCity') }}" required placeholder="City" autofocus>

                                    @error('orgCity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                          

                            <div class="form-group row">
                                <label for="orgLoc" class="col-md-4 col-form-label text-md-right">{{ __('organizationAddress') }}</label>

                                <div class="col-md-6">

                                <input id="orgLoc" type="text" class="form-control @error('orgLoc') is-invalid @enderror" name="orgLoc" value="{{ old('orgLoc') }}" required autocomplete="orgLoc" autofocus>

                                    @error('orgLoc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                         <!--  -->
                    
                        

                        <div class="form-group row">
                            <label for="visitReason" class="col-md-4 col-form-label text-md-right">{{ __('visitReason') }}</label>

                            <div class="col-md-6">

                                <select name="visitReason" id="college" class="form-control">
                                    <option value="For visiting">For Visiting</option>
                                    <option value="For Event">For Other Reasons</option>
                                    
                                </select>

                                @error('visitReason')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Next') }}
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

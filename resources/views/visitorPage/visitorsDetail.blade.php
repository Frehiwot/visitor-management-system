@extends('layouts.app')

@section('content')
<div class="container">
    @include('guardLayout.layout')

    <!-- today visitors -->

    <div class="card" id="tableCon">
        <div class="card-header">
             
        </div>
        <div class="card-body">
        <strong> </strong>
        
            <table class="table table-hover">

               <th>firstname</th><th>middlename</th><th>lastname</th><th>gender</th><th>country</th><th>city</th><th>Action</th>
               
               @foreach($form as $formm)
                  @if($formm->approved === 1)
                    @foreach($visitor as $visit)
                        @if($formm->vid === $visit->id)
                        <tr>
                            <td>{{ $visit->firstname }} </td>  
                            <td>{{ $visit->middlename }} </td> 
                            <td>{{ $visit->lastname }} </td> 
                            <td>{{ $visit->gender }} </td> 
                            <td>{{ $visit->country }} </td>
                            <td>{{ $visit->city }} </td> 
                            <td> <a href="/visitorDetail/{{ $visit->id }}" name="{{ $visit->id }}" id="btnn" class="btn btn-primary">Detail </a></td>
                            @if(count($send) == 0 )
                              <td> <a href="/sendNotification/{{ $visit->id }}" name="{{ $visit->id }}" id="btnn" class="btn btn-primary">sendNotification </a></td>
                           
                            @else
                                @foreach($send as $sends)
                                  @if($sends->vid == $visit->id)
                                  <td> <a href="#" name="{{ $visit->id }}" id="btnn" class="btn btn-primary">NotificationSend </a></td>
                                 
                                   @elseif($sends->vid != $visit->id)
                                     
                                     <small> {{ $notify++ }}</small>
                                  @endif
                                @endforeach
                                <!-- <h1> {{ $notify }} hello</h1> -->
                                @if($notify == count($send))
                                
                                  <small>{{$notify=0}}</small>
                                  
                                  <td> <a href="/sendNotification/{{ $visit->id }}" name="{{ $visit->id }}" id="btnn" class="btn btn-primary">sendNotification </a></td>
                                @else
                                <small>{{$notify=0}}</small>
                                @endif



                            
                            @endif

                           
                        <tr> 
                            
                        @endif
                    @endforeach
                  @endif
                @endforeach
              </table>
</div>
</div>
@endsection


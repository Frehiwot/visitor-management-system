@extends('layouts.app')

@section('content')
<div class="container">
@if($id)
    @include('staffLayout.layout')
@endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Visitation Information') }}</div>

                <div class="card-body">
                    <form method="POST" action="/groupvisitationFormSubmition/{{ $groupId }}" enctype="multipart/form-data">
                        @csrf
                       <fieldset>
                       
                          
                            <div class="form-group row">
                                <label for="college" class="col-md-4 col-form-label text-md-right">{{ __('College') }}</label>

                                <div class="col-md-6">

                                   <select name="college" id="college" class="form-control">
                                        @foreach($college as $colleges)
                                            <option value="{{ $colleges->name }}">{{ $colleges->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('college')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                           <div class="form-group row">
                                <label for="datee" class="col-md-4 col-form-label text-md-right">{{ __('visitDate') }}</label>

                                <div class="col-md-6">
                                    <input id="datee" type="date" class="form-control @error('datee') is-invalid @enderror" name="datee" value="{{ old('datee') }}" required autocomplete="datee" autofocus>

                                    @error('datee')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <label for="reason" class="col-md-4 col-form-label text-md-right">{{ __('Reason') }}</label>

                                <div class="col-md-6">
                                    <textarea name="reason" id="reason" cols="30" rows="10" class="form-control" placeholder="enter ur reason here"></textarea>
                                    @error('reason')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                          <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Next') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

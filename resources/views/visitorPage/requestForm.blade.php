@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Visitor Information') }}</div>

                <div class="card-body">
                    <form method="POST" action="/requestF" enctype="multipart/form-data">
                        @csrf
                       <fieldset>
                           <legend></legend>
                            <div class="form-group row">
                                <label for="name" class="col-md-1 col-form-label text-md-right"></label>

                                <div class="col-md-4">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" name="name"  placeholder="firstName" required  >

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <input id="middlename" type="text" class="form-control @error('middlename') is-invalid @enderror" name="middlename" value="{{ old('middlename') }}" placeholder="middleName"required >

                                    @error('middlename')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" placeholder="lastName" required >

                                    @error('lastname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                          

                            
                            
                            <div class="form-group row">
                                <label for="gender" class="col-md-1 col-form-label text-md-right"></label>

                                <div class="col-md-4">

                                    <select name="gender" id="" class="form-control">
                                        <option value="female">female</option>
                                        <option value="male">male</option>
                                        
                                    </select>

                                    @error('gender')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-4">

                                <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('country') }}" required autocomplete="country" placeholder="Country" autofocus>

                                    @error('country')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-3">

                                <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required placeholder="City" autofocus>

                                    @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        @if(!$id)
                         
                                <div class="form-group row">
                                    <label for="vtype" class="col-md-1 col-form-label text-md-right"></label>
                                    <div class="col-md-4">

                                            <select name="vtype" id="vtype" class="form-control">
                                                <option value="individual">individual</option>
                                                <option value="group">group</option>
                                            
                                            </select>

                                            @error('vtype')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="col-md-4">

                                        <input id="Age" type="number" class="form-control @error('Age') is-invalid @enderror" name="Age" value="{{ old('city') }}" required autocomplete="Age" placeholder="Age" autofocus>

                                            @error('Age')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        
                                    
                                </div>

                            @endif

                            

                            <div class="form-group row">
                                <label for="email" class="col-md-1 col-form-label text-md-right"></label>

                                <div class="col-md-4">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required  placeholder="email address" autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required  placeholder="phone number" autocomplete="phone">

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                @if($id)
                                <div class="col-md-3">

                                <input id="Age" type="number" class="form-control @error('Age') is-invalid @enderror" name="Age" value="{{ old('city') }}" required autocomplete="Age" placeholder="Age" autofocus>

                                    @error('Age')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                @endif
                            </div>


               
                          @if(!$id)
                                <div class="form-group row">
                                <label for="image" class="col-md-1 col-form-label text-md-right"></label>

                                <div class="col-md-4">
                                    <input id="image" type="file" class=" @error('image') is-invalid @enderror " name="image"  required >

                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            
                            </div>
                        @endif

                            

                            <div class="form-group row">
                                <label for="property" class="col-md-1 col-form-label text-md-right"></label>

                                <div class="col-md-4
                                
                                ">
                                   <input type="radio" name="property" value="yes" >I have Property <br />
                                   <input type="radio" name="property" value="no" >I donot have property

                                    @error('property')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </fieldset>
                        
                      <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Next') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

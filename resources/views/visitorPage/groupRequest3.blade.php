@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Visitor Information') }}</div>

                <div class="card-body">
                  
                        <form method="POST" action="/groupFinalRegistration/{{ $gid }}" enctype="multipart/form-data">
                            @csrf
                        <fieldset>
                            <input type="hidden" value="{{ $num }}" name="limit">
                            @for($i=0;$i < $num; $i++)
                             <strong>for user{{ $i+1 }}</strong>
                            <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('firstName') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name{{ $i+1 }}') is-invalid @enderror" name="name{{ $i+1 }}" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        @error('name{{ $i+1 }}')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="middlename" class="col-md-4 col-form-label text-md-right">{{ __('middleName') }}</label>

                                    <div class="col-md-6">
                                        <input id="middlename" type="text" class="form-control @error('middlename{{ $i+1 }}') is-invalid @enderror" name="middlename{{ $i+1 }}" value="{{ old('middlename') }}" required autocomplete="middlename" autofocus>

                                        @error('middlename{{ $i+1 }}')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('lastName') }}</label>

                                    <div class="col-md-6">
                                        <input id="lastname" type="text" class="form-control @error('lastname {{ $i+1 }}') is-invalid @enderror" name="lastname{{ $i+1 }}" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>

                                        @error('lastname{{ $i+1 }}')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                                    <div class="col-md-6">

                                        <select name="gender{{ $i+1 }}" id="">
                                            <option value="female">female</option>
                                            <option value="male">male</option>
                                            
                                        </select>

                                        @error('gender{{ $i+1 }}')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="age{{ $i+1 }}" class="col-md-4 col-form-label text-md-right">{{ __('Age') }}</label>

                                    <div class="col-md-6">

                                    <input id="age{{ $i+1 }}" type="number" class="form-control @error('age{{ $i+1 }}') is-invalid @enderror" name="age{{ $i+1 }}" value="{{ old('age') }}" required autocomplete="age" autofocus>

                                        @error('age{{ $i+1 }}')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Photo') }}</label>

                            <div class="col-md-6">
                                <input id="image{{ $i+1 }}" type="file" class="form-control @error('image{{ $i+1 }}') is-invalid @enderror" name="image{{ $i+1 }}"  required >

                                @error('image{{ $i+1 }}')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                      
                            
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email{{ $i+1 }}') is-invalid @enderror" name="email{{ $i+1 }}" value="{{ old('email') }}" required autocomplete="email">

                                @error('email{{ $i+1 }}')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-6">
                            <input id="phone" type="number" class="form-control @error('phone{{ $i+1 }}') is-invalid @enderror" name="phone{{ $i+1 }}" value="{{ old('phone') }}" required autocomplete="phone">

                                    @error('phone{{ $i+1 }}')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>
                            
                            @endfor
                               
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Next') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@component('mail::message')
# Introduction

# VisitorId

<img src="{{ asset('images/'.$visitor -> photo .'') }} "  width="100" height="100" >
<strong>Visitor Information</strong><br />
Full Name:{{ $visitor->firstname}} {{ $visitor->middlename}} {{ $visitor->lastname}}<br />
Id :{{ $visitor->id }} <br />
Age: {{ $visitor->age }} <br />
Email:{{ $visitor->email }} <br />
Phone : {{ $visitor->phone }} <br />
<strong>Visitation Detail</strong><br />
internId:{{ $intern-> id }} <br />
totalNumberOfInterns:{{ $intern-> NumberOfStudent }} <br />
GivingDepartment: {{ $intern -> department}} <br />
howLong: {{ $intern -> howLong}} <br />
valid: <strong>From {{ $intern->createdAt }} </strong>

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

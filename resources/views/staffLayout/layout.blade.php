<div  id="nowContainer">
    <nav >
    <a href="/home2" class="btn btn-primary now" > Home</a>
    <a href="/seeRequest" class="btn btn-primary now" > seeRequest</a>
    <a href="#" class="btn btn-primary now" id="mainRegister"> createRequest</a>
    <div id="registerLinks">
        <!-- <a href="/forIntern" class="btn btn-primary now insideBtn"> For Intern</a>
        <a href="/training" class="btn btn-primary now insideBtn"> For Training</a>
        <a href="/forEvent" class="btn btn-primary now insideBtn"> For Event</a>
        <a href="/visitorRegister" class="btn btn-primary now insideBtn"> Individual</a>
        <a href="/staffGroupRequest" class="btn btn-primary now insideBtn"> Group</a> -->
        <form method="post" action="/identifyRequest" class="insideBtnn">
        @csrf
            <select name="choice" id="" class="form-control">
                <option value="Individual">Individual</option>
                <option value="Training">Training</option>
                <option value="Intern">Intern</option>
                <option value="group">group</option>
            </select>
            <input type="submit" value="Submit" class="btn btn-primary">
        </form>
   </div>
    <a href="/notifications"  class="btn btn-primary now"> seeNotifications</a>
    <a href="#"  class="btn btn-primary now" id="mainMessage"></span> putMessage</a>
    <div id="messageLinks">
        <a href="/message/1" class="btn btn-primary now insideBtn"> I am available</a>
        <a href="/message/2" class="btn btn-primary now insideBtn"> I am not available</a>
        <a href="/addtional" class="btn btn-primary now insideBtn"> addtional information</a>
   </div>
    
    @guest
                    
        @if (Route::has('register'))
            
        @endif
        @else
            <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                @csrf
                <input type="submit" value="logout" class="btn btn-primary now" id="logoutBtn">
            </form>
                    
    @endguest
    
            <mine-component v-bind:notifications="notifications"></mine-component>
        
    </li>
  
    </nav>
</div>
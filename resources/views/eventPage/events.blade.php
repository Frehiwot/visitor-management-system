@extends('layouts.app')

@section('content')
<div class="container">

    @include('guardLayout.layout')


<div id="nowContainer2">
    <div id="search" class="form-group row">
        <div class="col-md-8"></div>
        <input type="search" name="filter" id="filter" placeholder="filter events" class="form-control col-md-4">
     </div>

</div>
<div class="header" id="eventContainer" >
   
@if(count($event) >= 1)
    

    @foreach($event as $events)
           
            
        <div class="card">
        <div class="card-header">
                Event Id:- {{ $events->id }}
            </div>
            <div class="card-body">
                    <strong>type</strong>:{{$events -> type }} <br /><br />
                    <strong>hostedBy</strong>: {{ $events->department }}<br /><br />
                    <strong>startDate</strong>:{{ $events->startDate }}<br /><br />
                    <strong>endDate</strong>:{{ $events->endDate }}<br /><br />
                    <strong>description</strong>:{{$events -> description }}<br /><br />
                    <strong>allowed</strong>:{{ $events -> allowed }}<br /><br />

                    <br/> <br />
                    
            </div>  
        </div>
       
        @endforeach
    @elseif(count($event) == 0)
        <h1>no event</h1>
    @endif
    </div>
    

@endsection

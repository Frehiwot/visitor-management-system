<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class notifingInterns extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $visitor;
    public $intern;
    public $datee;
    public function __construct(\App\visitor $visitor,\App\intern $intern)
    {
        $this->visitor=$visitor;
        $this->intern=$intern;
        // $this->datee=$datee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('notifingInterns');
    }
}

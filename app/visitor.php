<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class visitor extends Model
{
    protected $fillable=[
        'photo' ,
         'firstname' ,
         'middlename' ,
         'lastname',
         'gender' ,
         'email' ,
         'phone' ,
         'country' ,
         'city' ,
         'age'  
        
    ];
}

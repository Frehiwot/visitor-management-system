<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class internController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function listIntern()
    {
        // $internn=\App\intern::all();
          $internn=\App\intern::all()->where('startDate','<=',date('Y-m-d'));
        // $finalInten=$internn->where()
        $intern=\App\internship::all();
        // $x=$inter;
        // $datee=date('Y-m-d',strtotime("+$x days")) ;
        
        $today=Carbon::now();
        $visitor=\App\visitor::all();
        return view('guardPage.listIntern')->with([
            'intern' => $intern,
            'visitor' => $visitor,
            'internn' => $internn,
            'today' =>$today
        ]);
    }
}

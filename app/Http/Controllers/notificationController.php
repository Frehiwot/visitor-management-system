<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class notificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function get()
    {
        $notification=Auth::user()->unreadNotifications;
        return $notification;
    }
    public function read(Request $request)
    {
       Auth::user()->unreadNotifications()->find($request->id)->markAsRead();
       return 'success';
    }
}

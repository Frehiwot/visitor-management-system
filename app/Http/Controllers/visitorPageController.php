<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class visitorPageController extends Controller
{
    public function viewVisitor()
    {
        $id=auth()->id();
        return view('visitorPage.requestForm',compact('id'));
    }
    public function visitForm()
    {
        $min=date('Y-m-d');
        return view('visitorPage.visitationForm',compact('min'));
    }
    public function expand($id)
    {
        $visitor=\App\visitor::find($id);
        return view('staffPage.visitorDetail')->with([
            'visitor' => $visitor

        ]);

    }
    public function destroy($id)
    {
        $vid=$id;
        
        $visitor=\App\visitor::findorFail($id)->delete();
        $property=\App\property::all()->where('vid','=',$vid);
        if(count($property)>=1)
        {
            foreach($property as $properties)
            {
                
                // $propertiess=\App\property::findorFail($id)->delete();

            }
        }
        return redirect('/seeRequest');
       
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\staffController;
use Carbon\Carbon;
use Auth;

class staffController extends Controller
{
    
    public function notifyExpand($id)
    {
        $user=\App\User::findorFail(auth()->id());
        $notify=\App\notification::where('id','=',$id)->first() ;
       
        // foreach($notifes as $notify)
        // {
            $rid=$notify->data['rid'];

            
            // Auth::user()->unreadNotifications()->find($id)->markAsRead();
            
            // $notify->read=1;
            // $notify->save();
            $path='images/'.'AAU.JPG';
            
            if($notify->data['type'] == "individual" || $notify->data['type'] == "notify" || $notify->data['type'] == "individualEmergency")
            {

                
                $reque=\App\request_form::findorFail($rid);
                $vid=$reque->vid;
                $visitor=\App\visitor::findorFail($vid);
                
            
                
            $staff=\App\User::where('staffId','=',$reque->staffid)->firstorFail();
            
           

               if($reque->approved == 0)
                {
               
                    if($user->type == "staff"){
                        return view('staffPage.staffInterface2')-> with([
                            'form' => $reque,
                            'visitor' => $visitor,
                            'path' => $path,
                            'approved' => $reque->approved,
                            'type' => $notify->data['type']
                        ]);
                    }
                   
                }
                else{
                    if($user->type == "guard")
                    {
                        return view('guardPage.guardNotificationPage')-> with([
                            'form' => $reque,
                            'visitor' => $visitor,
                            'staff' => $staff,
                            'path'=>'images/AAU.jpg',
                            'approved' => $reque->approved
                        ]);
                    }
                    else{
                        
                        return view('staffPage.staffInterface2')-> with([
                            'form' => $reque,
                            'visitor' => $visitor,
                            'staff' => $staff,
                            'path'=>'images/AAU.jpg',
                            'approved' => $reque->approved,
                            'type' => $notify->data['type']
                        ]);
                    }
                    
                }
            }
            else if($notify->data['type'] == "intern"){

               
    
                $intern=\App\intern::findorFail($rid);
    
                $staff=\App\User::where('staffId','=',$intern->staffId)->first();
                $representative=\App\User::findorFail(auth()->id());
                $internship=\App\internship::all()->where('internId','=',$intern->id);
                $visitor=\App\visitor::all();
                $approval=\App\approve::all()->where('requestId','=',$intern->id);
                $approved=$approval->where('type','=','intern');
                $userr=\App\User::all()->where('type','=','Staff');
                if($user->type == "guard")
                {
                    return view('guardPage.guardInternPage') ->with([
                        'intern' => $intern,
                        'staff' => $staff,
                        'internship' => $internship,
                        'visitor' => $visitor,
                        'rep' =>  $representative,
                        'approve' => $approved,
                        'user' => $userr
                    ]);
                }
                return view('staffPage.staffInternPage') ->with([
                    'intern' => $intern,
                    'staff' => $staff,
                    'internship' => $internship,
                    'visitor' => $visitor,
                    'rep' =>  $representative,
                    'approve' => $approved,
                    'user' => $userr
                ]);
    
            }
            else if($notify->data['type'] == "group"){
                $visitor=\App\visitor::all();
                $staff=\App\User::findorFail(auth()->id());
                $req=\App\request_form::findorFail($rid);
                $vid=$req->vid;
                
                
                if($req->rep == "staff")
                {

                    $rep=\App\User::where('id','=',$vid)->first();
                   
                }
                elseif($req->rep == "visitor")
                {
                    $rep=\App\visitor::where('id','=',$vid)->first();
                }
                
                $groups=\App\group::latest('created_at')->get();
                $group=$groups->where('vid','=',$vid)->first();
                
                
                $organization=\App\organization::where('gid','=',$group->id)->first();
                //check whether this is empty or not
                
                
                $limit=$group->largestAge;
                if($limit >= 18)
                {
                    $grouprep=\App\group_rep::all()->where('groupId','=',$group->id);
                   
                   
                }
                return view('staffPage.staffGroupPage') ->with([
                    'group' => $group,
                    'groupRep' => $grouprep,
                    'form' => $req,
                    'visitor'=> $visitor,
                    'staff'=> $staff,
                    'rep'=> $rep,
                    'approved' => $req->approved,
                    'org' =>  $organization
                   
                ]);
    
            }
        
        
        
       
    }

    
    public function approve($id)
    {
        $form=\App\request_form::findorFail($id);
        $form->approved=1;
       

        $form->save();
        $visitor=\App\visitor::findorFail($form->vid);

         // mail to the visitor the appropriate informTION

         \Mail::to($visitor->email)->send(
            new \App\Mail\notifingVisitor($visitor,$form )
        );

        //notifying the guard about the approval
        $user=\App\User::where('type','=','guard')->first();

        $approve=new \App\approve();
        $approve->requestId=$form->id;
        $approve->approvedby=$form->staffid;
        $approve->approveDate=Carbon::now();    //make it current timestamp
        $approve->save();

        \App\User::find($user->id)->notify(new \App\Notifications\visitorRequest($user->staffId,$form->id,'individual'));

        $staff=\App\User::where('staffId','=',$form->staffid)->firstorFail();

        session()->flash('message','u have approved this visitaion request');

        return view('staffPage.staffInterface2')-> with([
            'form' => $form,
            'visitor' => $visitor,
            'staff' => $staff,
            'path'=>'images/AAU.jpg',
            'approved' => $form->approved
        ]);
    }


    public function generate($id,$nid)
    {
        $internship=\App\intern::findorFail($id);
        $num=$internship->count;
        $num=$num-2;
        $i=1;
        if($internship->count >1)
        {
            $internship->count=$nid;
            $internship->save();

            for($i=1;$i<=1;$i++)
            {
                $visitor=new \App\visitor();
                $name='name'.$i;
                $middlename='middlename'.$i;
                $lastname='lastname'.$i;
                $gender='gender'.$i;
                $age='age'.$i;
                $phone='phone'.$i;
                $email='email'.$i;
                $visitor->firstname=request($name);
                $visitor->middlename=request($middlename);
                $visitor->lastname=request($lastname);
                $visitor->gender=request($gender);
                $visitor->age=request($age);
                $visitor->phone=request($phone);
                $visitor->email=request($email);
                $visitor->country='ethiopia';
                $visitor->city='addis ababa';
                $visitor->photo='images/AAU.jpg';
                $visitor->save();

                $intern=new \App\internship();
                $intern->vid=$visitor->id;
                $intern->internId=$id;
                $intern->save();
            }
           
        }
        else if($internship->count <=1)
        {
            for($i=1;$i<=$internship->count;$i++)
            {
                $visitor=new \App\visitor();
                $name='name'.$i;
                $middlename='middlename'.$i;
                $lastname='lastname'.$i;
                $gender='gender'.$i;
                $age='age'.$i;
                $phone='phone'.$i;
                $email='email'.$i;
                $visitor->firstname=request($name);
                $visitor->middlename=request($middlename);
                $visitor->lastname=request($lastname);
                $visitor->gender=request($gender);
                $visitor->age=request($age);
                $visitor->phone=request($phone);
                $visitor->email=request($email);
                $visitor->country='ethiopia';
                $visitor->city='addis ababa';
                $visitor->photo='images/AAU.jpg';
                $visitor->save();

                $intern=new \App\internship();
                $intern->vid=$visitor->id;
                $intern->internId=$id;
                $intern->save();
            }
            session()->flash('message','we will notify u');
            // sending notification table
            $department=$internship->department;
            $departmentId=\App\department::where('name','=',$department)->firstorFail();
            //send notification for the department representative

            // \App\notification::create([
            //   'sid'=>$departmentId->representative,
            //    'rid'=>$internship->id,
            //    'notification'=>'u have internship approval request',
            //    'type' => 'intern',
            //    'read'=> 0
            // ]);
            $currentUser=\App\User::find(auth()->id());
            if($currentUser->staffId == $departmentId->representative)
            {
                return redirect('/internApproving'.$id);
            }
            else{
                $user=\App\User::where('staffId','=',$departmentId->representative)->first();
            
                \App\User::find($user->id)->notify(new \App\Notifications\visitorRequest($departmentId->representative,$internship->id,'intern'));
    
                return view('visitorPage.messagePage');
            }
            
        }


        return view('staffPage.internFormGenerator',compact('internship'));
    }
    public function forIntern()
    {
        $department=\App\department::all();
        $min=date('Y-m-d');
        return view('staffPage.internForm1')->with([
            'department' => $department,
            'min' => $min
        ]);
    }
    public function internRegister()
    {
        
        request()->validate([
            'numofStudent' => ['required'],
            'department' => ['required'],
            'dates' => ['required'],
            'datee' => ['required']

        ]);
        $staff=\App\User::findorFail(auth()->id());

       $intern=new \App\intern();
       $intern->department=request('department');
       $intern->howLong=request('dates');
       $intern->NumberOfStudent=request('numofStudent');
       $intern->staffId=$staff->staffId;
       $intern->count=request('numofStudent');
       $intern->startDate=request('datee');
       $intern->save();

       $internship=\App\intern::findorFail($intern->id);

       return view('staffPage.internFormGenerator',compact('internship'));


    }

  

    public function message($id)
    {
        $user=\App\User::findorFail(auth()->id());

        $sid=$user->staffId;
        $notifyUnread=\App\notification::all()->where('sid','=',$sid);
        $notify=$notifyUnread->where('read','=',0);
    //    return $notify;

        $message=new \App\message();
        $message->sid=$sid;
        $message->datee=Carbon::now();
        $message->messageType="availablity";
        $message->messageDate=date("Y-m-d");
        if($id == 1)
        {
           
            $message->message="I am Available";
            $message->save();
        }
        elseif($id== 2)
        {
            $message->message="I am not Available";
            $message->save();
        }
       
        
       return redirect('/home2');
        


    }
    public function addtionMessage()
    {
        $user=\App\User::findorFail(auth()->id());

        $sid=$user->staffId;
        // $notifyUnread=\App\notification::all()->where('sid','=',$sid);
        // $notify=$notifyUnread->where('read','=',0);
    //    return $notify;

        $message=new \App\message();
        $message->sid=$sid;
        $message->datee=Carbon::now();
        $message->messageType=request('subject');
        $message->messageDate=date("Y-m-d");
        
           
        $message->message=request('message');
        $message->save();
        
       
        
       return redirect('/home2');
    }

    public function internApprove($id)
    {
        
        $intern=\App\intern::findorFail($id);
        $num=$intern->approved;
        $intern->approved=$num + 1;
        $intern->save();

        $staff=\App\User::where('staffId','=',$intern->staffId)->first();
        $representative=\App\User::findorFail(auth()->id());
        

        $approve=new \App\approve();
        $approve->requestId=$intern->id;
        $approve->approvedby=$representative->id;
        $approve->approveDate=Carbon::now();    //make it current timestamp
        $approve->type="intern";
        $approve->save();
        


        $approval=\App\approve::all()->where('requestId','=',$intern->id);
        $approved=$approval->where('type','=','intern');
        $user=\App\User::all()->where('type','=','Staff');
        
        $internship=\App\internship::all()->where('internId','=',$intern->id);
        $visitor=\App\visitor::all();
        

        if($intern->NumberOfStudent>3 && $intern->approved == 1)
        {
            
            $department=$intern->department;
            $departmentId=\App\department::where('name','=',$department)->firstorFail();
            //send notification for the department representative
            $college=$departmentId->college;
            
            $collegeId=\App\college::where('name','=',$college)->firstorFail();
            
            
            //mail for interns

            $user=\App\User::where('staffId','=',$collegeId->represenative)->first();
            
            \App\User::find($user->id)->notify(new \App\Notifications\visitorRequest($collegeId->represenative,$internship->id,'intern'));
        }
        else{
            //mailling
            
            $user=\App\User::where('type','=','guard')->first();
            
            \App\User::find($user->id)->notify(new \App\Notifications\visitorRequest($user->staffId,$intern->id,'intern'));

            //sending email,makes it slower,so try to find another way
            
            // foreach($internship as $interns)
            // {
                
            //     $visitor=\App\visitor::findorFail($interns->vid);
                
                
            //     \Mail::to($visitor->email)->send(
            //         new \App\Mail\notifingInterns($visitor,$intern )
                    
            //     );
                
                
            // }
            
        }
        // return $id;


        // send has approved notification for the one who request it

        return view('staffPage.staffInternPage') ->with([
            'intern' => $intern,
            'staff' => $staff,
            'internship' => $internship,
            'visitor' => $visitor,
            'rep'=>$representative,
            'approve'=> $approved,
            'user' => $user
        ]);

        
        

        

    }
    public function internDisapprove($id)
    {
        $intern=\App\intern::findorFail($id);
        $intern->approved=0;
        $intern->save();

    }

    public function notification()
    {
        $user=\App\User::findorFail(auth()->id());
        $department=\App\department::where('name','=',$user->department)->first();
        // change it by laravel notification
       
        $notification=\App\notification::all()->where('notifiable_id','=',$user->id); 
      
        if($department->representative == $user->staffId)
        {
            return view('staffPage.notificationPage')->with([
                'notify' => $notification,
                'user' => $user,
                'rep' => 1
            ]);
        }
        else{
            return view('staffPage.notificationPage')->with([
                'notify' => $notification,
                'user' => $user,
                'rep' => 0
            ]);
        }
        
    }
    public function groupApprove($id)
    {
        $req=\App\request_form::findorFail($id);
        $num=$req->approved;
        $req->approved=$num + 1;
        $req->save();

        // $staff=\App\User::where('staffId','=',$intern->staffId)->first();
        $representative=\App\User::findorFail(auth()->id());

        $approve=new \App\approve();
        $approve->requestId=$req->id;
        $approve->approvedby=$representative->id;
        $approve->approveDate=Carbon::now();    //make it current timestamp
        $approve->type="group";
        $approve->save();

        // send has approved notification for the one who request it

        $visitor=\App\visitor::all();
            $staff=\App\User::findorFail(auth()->id());
            
            $vid=$req->vid;

            $rep=\App\visitor::where('id','=',$vid)->first();

            if($req->rep == "staff")
            {
                $rep=\App\User::where('id','=',$vid)->first();
            }
            elseif($req->rep == "visitor")
            {
                $rep=\App\visitor::where('id','=',$vid)->first();
            }
            
            $groups=\App\group::latest('created_at')->get();
            $group=$groups->where('vid','=',$vid)->first();
            
            $organization=\App\organization::where('gid','=',$group->id)->first();
            //check also the smallest limit
            
            $limit=$group->largestAge;
            if($limit >= 18)
            {
                $grouprep=\App\group_rep::all()->where('groupId','=',$group->id);
               
            }
            
            return view('staffPage.staffGroupPage') ->with([
                'group' => $group,
                'groupRep' => $grouprep,
                'form' => $req,
                'visitor'=> $visitor,
                'staff'=> $staff,
                'rep'=> $rep,
                'approved' => $req->approved,
                'org' =>  $organization
               
            ]);
        }
        public function timeSet($id)
        {
            
            request()->validate([
             'setTime' => ['required']
            ]);
            $form=\App\request_form::findorFail($id);
            $form->approvedTime=request('setTime');
            $form->save();

            $vid=$form->vid;
            $visitor=\App\visitor::findorFail($vid);
            $path='images/'.'AAU.JPG';

            return view('staffPage.staffInterface2')-> with([
                'form' => $form,
                'visitor' => $visitor,
                'path' => $path,
                'approved' => $form->approved
            ]);

        }
        public function addtionalMessage()
        {
            return view('staffPage.addtionalMessage');
        }
   
    

    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class eventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function events()
    {
        $events=\App\event::all()->where('startDate','<=',date('Y-m-d H:m:s'));
        $event=$events->where('endDate','>=',date('Y-m-d H:m:s'));
        
        $date=date('Y-m-d');
        //check the time setting
        
        return view('eventPage.events')->with([
         'currentDate' => Carbon::now(),
          'event' => $event
        ]);
    }
    public function forEvent()
    {
        $department=\App\department::all();
        return view('staffPage.staffEvent',compact('department'));
    }
    public function eventRegister()
    {
        $user=\App\User::find(auth()->id());
        
        request()->validate([
         'eventName' => ['required'],
         'type' => ['required'],
         'description' => ['required'],
         'datee' => ['required'],
         'endDate' => ['required']
        ]);
        $event=new \App\event();
        $event->type=request('type');
        $event->eventName=request('eventName');
        $event->startDate=request('datee');
        $event->endDate=request('endDate');
        $event->allowed=request('allowed');
        $event->department=request('department');
        $event->description=request('description');
       
        $event->sid=$user->staffId;
       
        $event->available=1;
        $event->save();
        //check if approval i needed
        session()->flash('message','ur event will be posted');
        return view('visitorPage.messagePage');
    }
    //form generator
    public function eventRegisterr($id)
    {
        $visitor=new \App\visitor();
        // make country,city,email nullable
        $visitor->firstname=request('name');
        $visitor->middlename=request('middlename');
        $visitor->lastname=request('lastname');
        $visitor->email=request('email');
        $visitor->phone=request('phone');
        $visitor->save();
    }
}

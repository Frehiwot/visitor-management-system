<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class registerController2 extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function create()
    {
        $attribute=request()->validate([
            'department' => 'required',
            'id' => 'required',
            'role' => 'required'
        ]);
        $staff=new \App\staff_member();
        $staff->uid=request('idd');
        $staff->college="ncs";
        $staff->department=request('department');
        $staff->sid=request('id');

        $staff->save();
        return view('homeNotify');
    }
    public function visitorRegister()
    {
        $department=\App\department::all();
        $college=\App\college::all();
        $staff=\App\User::all();
        $min=date('Y-m-d');
        return view('guardPage.register')->with([
            'department' => $department,
            'college' => $college,
            'staff' => $staff,
            'min'=> $min
        ]);
    }
    public function viewVisitor()
    {
        $id=auth()->id();
        return view('visitorPage.requestForm',compact('id'));
    }
    public function visitForm()
    {
        $min=date('Y-m-d');
        return view('visitorPage.visitationForm',compact('min'));
    }
   
    public function guardInterface1()
    {
        $user=\App\User::findorFail(auth()->id());
        $id=$user->staffId;
        $department=\App\department::all()->where('college','=',$user->college);
            
            $college=\App\college::where('name','=',$user->college)->first();
            $staff=\App\User::all()->where('type','=','staff');

            return view('guardPage.Interface1')->with([
                'department' => $department,
                'college' => $college,
                'staff' => $staff
            ]);
    }
    public function registering()
    {
        // request()->validate([
        //     'name' => ['required', 'string', 'min:3','max:255'],
        //     'middlename' => ['required', 'string', 'min:3','max:255'],
        //     'lastname' => ['required', 'string', 'min:3','max:255'],
        //     'gender' => ['required'],
        //     // 'country' => ['required','min:3'],
        //     // 'city' => ['required','min:3'],
        //     'Age' => ['required'],
        //     // 'property' => ['required'],
        //     'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        //     'phone' => ['required', 'min:10','max:10'],
        //     // 'college' => ['required'],
        //     'reasons' => ['required'],
        //     'datee' => ['required'],
        //     'reason' => ['required'],
            
        //     'department' => ['required'],
        // ]);

        $visitor=new \App\visitor();
        
           
        $visitor->firstname = request('name');
        $visitor ->middlename = request('middlename');
        $visitor->lastname=request('lastname');
        $visitor->gender= request('gender');
        $visitor->email= request('email');
        $visitor->phone= request('phone');
        
        $visitor->age= request('Age') ;
        $visitor->save();

        $reques=new \App\request_form();
        
            
        $department=\App\department::where('name','=',request('department'))->first();
       
        $reques->vid=$visitor->id;
        $reques->college=$department->college;
        $reques->whomToVisit=request('reasons');
        $reques->visitationDate=date('Y-m-d');
        //  correct the request date
        $reques->requestDate=date('Y-m-d');
        $reques->Reason=request('reason');
        $reques->departmentName=request('department');
        $reques->type='individual';
        

        if(request('reasons') == "staff")
        {

            $reques->staffId=$staff;
        }
        else
        {
            $reques->staffId=$department->representative;
        }
        $reques->save();

        if(request('reasons') == "staff")
            {
                \App\User::find($staffId)->notify(new \App\Notifications\visitorRequest($staffUser->staffId,$reques->id,'individualEmergency'));

            }
            elseif(request('reasons') == "Department")
            {
                $user=\App\User::where('staffId','=', $department->representative)->first();
                \App\User::find($user->id)->notify(new \App\Notifications\visitorRequest($user->staffId,$reques->id,'individualEmergency'));

            }
            
            session()->flash('message','thank u for ur collaboration,we have send ur request to the appropriate body,until then ..');
            return view('visitorPage.messagePage');
    }
    public function registerAll()
    {
        request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'middlename' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'gender' => ['required'],
            'type' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required',  'max:10'],
            'department' => ['required'],
            'college' => ['required'],
            'id' => ['required','min:10','max:11'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        if(request('department') != "none")
        {
            $department=\App\department::where('name','=',request('department'))->first();
            $college=\App\college::where('name','=',$department->college)->first();
        }
        else{
            $college=request('college');
        }
        
    

        \App\User::create([
            'firstname' => request('name'),
            'middlename' => request('middlename'),
            'lastname' => request('lastname'),
            'gender' => request('gender'),
            'email' => request('email'),
            'phone' => request('phone'),
            'type' => request('type'),
            'college' => $college,
            'department' => request('department'),
            'staffId' => request('id'),
            'password' => Hash::make(request('password')),
        ]);
     

        if(request('type') == "admin")
        {
            $user=\App\User::findorFail(auth()->id())->delete();
            session()->flash('message','u have registered the new admin succesfully');
            return view('adminPage.messagePage');
        }

        session()->flash('message','u have registered succesfully');
        return view('adminPage.messagePage');
    }
    public function storeDepartment()
    {
         request()->validate([
          'depName' => ['required','min:3'],
          'rep' => ['required','min:10'],
          'location' => ['required'],
          'parent' => ['required'],
          'parentName' => ['required']
         ]);

         $department=new \App\department();
         $department->name=request('depName');
         $staff=\App\User::where('staffId','=',request('rep'))->first();
         
         $department->representative=request('rep');
         if(request('parent') == "college")
         {
            $department->college=request('parentName');
         }
         $department->location=request('location');
         $department->save();
         
         if(!$staff)
         {
             session()->flash('message','u have registered succesfully.but u are trying to register unexisting rep,so please register the rep to make the data appropriate');
             return view('adminPage.messagePage');
         }
         else{
            session()->flash('message','u have registered succesfully');
            return view('adminPage.messagePage');
         }
         return redirect('/home2');

         
    }

    public function storeCollege()
    {
         request()->validate([
          'colName' => ['required','min:3'],
          'rep' => ['required','min:10'],
          'location' => ['required'],
          'parent' => ['required'],
          'parentName' => ['required']
         ]);

         $college=new \App\college();
         $college->name=request('colName');
         $college->representative=request('rep');
         $staff=\App\User::where('staffId','=',request('rep'))->first();
        
         if(request('parent') == "university")
         {
            $college->university=request('parentName');
         }
         $college->location=request('location');
         $college->save();
         if(!$staff)
         {
             session()->flash('message','u are trying to register unexisting rep,so please register the rep to make the data appropriate');
             return view('adminPage.messagePage');
         }
         return redirect('/home2');

         
         
    }
    
   
}

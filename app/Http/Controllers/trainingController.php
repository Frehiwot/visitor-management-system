<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class trainingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function createTraining()
    {
        $department=\App\department::all();
        $college=\App\college::all();
        $min=date('Y-m-d');

        return view('trainingPage')->with([
            'department' => $department,
            'college' => $college,
            'min' => $min
        ]);
    }
    public function store()
    {
        
        request()->validate([
          'trainName' => ['required'],
          'college'=> ['required'],
          'department' => ['required'],
          'starts'=>['required'],
          'ends' => ['required'],
          'quantity' => ['required','min:1']
        ]);
        $training=new \App\training();
        $training->name=request('trainName');
        $training->college=request('college');
        $training->department=request('department');
        $training->numberofTrainies=request('quantity');
        $training->startDate=request('starts');
        $training->count=request('quantity');
        $training->endDate=request('ends');
        $training->save();

        

        if(request('day1'))
        {
            $datee=new \App\training_date();
            $datee->tid=$training->id;
            $datee->datee=request('day1');
            $datee->save();
        }
        if(request('day2'))
        {
            $datee=new \App\training_date();
            $datee->tid=$training->id;
            $datee->datee=request('day2');
            $datee->save();
        }
        if(request('day3'))
        {
            $datee=new \App\training_date();
            $datee->tid=$training->id;
            $datee->datee=request('day3');
            $datee->save();
        }
        if(request('day4'))
        {
            $datee=new \App\training_date();
            $datee->tid=$training->id;
            $datee->datee=request('day4');
            $datee->save();
        }
        if(request('day5'))
        {
            $datee=new \App\training_date();
            $datee->tid=$training->id;
            $datee->datee=request('day5');
            $datee->save();
        }
        return view('trainingFormGenerator',compact('training'));

}
    public function generateTraining($id,$nid)
    {
        $training=\App\training::findorFail($id);
        $num=$training->count;
        $num=$num-2;
        $i=1;
        if($training->count >1)
        {
            $training->count=$nid;
            $training->save();

            for($i=1;$i<=1;$i++)
            {
                $visitor=new \App\visitor();
                $name='name'.$i;
                $middlename='middlename'.$i;
                $lastname='lastname'.$i;
                $gender='gender'.$i;
                $age='age'.$i;
                $phone='phone'.$i;
                $email='email'.$i;
                $visitor->firstname=request($name);
                $visitor->middlename=request($middlename);
                $visitor->lastname=request($lastname);
                $visitor->gender=request($gender);
                $visitor->age=request($age);
                $visitor->phone=request($phone);
                $visitor->email=request($email);
            
                $visitor->save();

                $trainer=new \App\trainer();
                $trainer->vid=$visitor->id;
                $trainer->tid=$id;
                $trainer->save();
            }
           
        }
        else if($training->count <=1)
        {
            for($i=1;$i<=$training->count;$i++)
            {
                $visitor=new \App\visitor();
                $name='name'.$i;
                $middlename='middlename'.$i;
                $lastname='lastname'.$i;
                $gender='gender'.$i;
                $age='age'.$i;
                $phone='phone'.$i;
                $email='email'.$i;
                $visitor->firstname=request($name);
                $visitor->middlename=request($middlename);
                $visitor->lastname=request($lastname);
                $visitor->gender=request($gender);
                $visitor->age=request($age);
                $visitor->phone=request($phone);
                $visitor->email=request($email);
                $visitor->country='ethiopia';
                $visitor->city='addis ababa';
                $visitor->photo='images/AAU.jpg';
                $visitor->save();

                $trainer=new \App\trainer();
                $trainer->vid=$visitor->id;
                $trainer->tid=$id;
                $trainer->save();
            }
            session()->flash('message','we have generated ur request');
            // sending notification table
            // $department=$internship->department;
            // $departmentId=\App\department::where('name','=',$department)->firstorFail();
            //send notification for the department representative

            // \App\notification::create([
            //   'sid'=>$departmentId->representative,
            //    'rid'=>$internship->id,
            //    'notification'=>'u have internship approval request',
            //    'type' => 'intern',
            //    'read'=> 0
            // ]);
            $user=\App\User::where('type','=','guard')->first();
            
            \App\User::find($user->id)->notify(new \App\Notifications\visitorRequest($user->staffId,$training->id,'training'));

            return view('visitorPage.messagePage');
        }


        return view('trainingFormGenerator',compact('training'));
    }

    public function listTraining()
    {
        $train=\App\training::all()->where('startDate','<=',date('Y-m-d H:m:s'));
        $trains=$train->where('endDate','>=',date('Y-m-d H:m:s'));
       
        $trainers=\App\trainer::all();
        $date=\App\training_date::all();
    
        
        $x=20;
       // $datee=date('Y-m-d',strtotime("+$x days")) ;
        
        // $today=Carbon::now();
        $visitor=\App\visitor::all();
        return view('listTraining')->with([
            'training' => $trains,
            'visitor' => $visitor,
            'trainers' => $trainers,
            // 'today' =>$today,
            'date' => $date
        ]);
    }
    public function listingTrainers($id)
    {
        $trainers=\App\trainer::all()->where('tid','=',$id);
        $visitor=\App\visitor::all();
        return view('listingTrainers')->with([
         'trainers' => $trainers,
         'visitor' => $visitor
        ]);
    }
    
}

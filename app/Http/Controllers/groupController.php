<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class groupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function group()
    {
        $group=\App\group::all();
        $formm=\App\request_form::all()->where('approved','>=',1);
        $form=$formm->where('visitationDate','=',date('Y-m-d'));
        
        $rep=\App\group_rep::all();
        
        $visitor=\App\visitor::all();
        return view('guardPage.listGroup')->with([
            'group' => $group,
            'form' => $form,
            'rep' => $rep,
            'visitor' => $visitor
        ]);
    }
    public function groupDetail($id,$gid)
    {
        $req=\App\request_form::findorFail($id);
        

        // $staff=\App\User::where('staffId','=',$intern->staffId)->first();
        $representative=\App\User::findorFail(auth()->id());

       

        // send has approved notification for the one who request it

        $visitor=\App\visitor::all();
            $approvedBy=\App\approve::all()->where('requestId','=',$req->id);
            
            $staff=\App\User::all();
            
            $vid=$req->vid;

            $rep=\App\visitor::where('id','=',$vid)->first();

            if($req->rep == "staff")
            {
                $rep=\App\User::where('id','=',$vid)->first();
            }
            elseif($req->rep == "visitor")
            {
                $rep=\App\visitor::where('id','=',$vid)->first();
            }
            
            $group=\App\group::findorFail($gid);
           
            
            $organization=\App\organization::where('gid','=',$group->id)->first();
            //check also the smallest limit
            
            $limit=$group->largestAge;
            if($limit >= 18)
            {
                $grouprep=\App\group_rep::all()->where('groupId','=',$group->id);
               
            }
            
            return view('guardPage.guardGroupPage') ->with([
                'group' => $group,
                'groupRep' => $grouprep,
                'form' => $req,
                'visitor'=> $visitor,
                'approvedBy'=> $approvedBy,
                'rep'=> $rep,
                'approved' => $req->approved,
                'org' =>  $organization,
                'staff' => $staff
               
            ]);
        
        
    }
}

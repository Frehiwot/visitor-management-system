<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GroupRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function staffGroupRequest()
    {
        return view('staffPage.staffGroupRequest');
    }
    public function groupRegistration()
    {
        
        request()->validate([
         'orgType' => ['required'],
         'orgName' => ['required','min:3','max:255'],
         'orgCountry' => ['required','min:3','max:255'],
         'orgCity' => ['required','min:3','max:255'],
         'ageSmallestLimit' => ['required'],
         'ageLargestLimit' => ['required'],
         'howMany' => ['required'],
         'quantity'=> ['required'],
         'quantityF'=> ['required'],
         'visitReason'=> ['required'],
         'orgLoc' => ['required'],
         
         
        ]);
        $total=request('quantityF') + request('quantity');
        if(request('howMany') > $total)
        {
            return "error has happened";
        }
       
        //for this we use the rep as the staff
        $visitor=\App\User::findorFail(auth()->id());
        

        $group=new \App\group();
        $group->vid=$visitor->id;
        $group->numberOfFemale=request('quantityF');
        $group->numberOfMale=request('quantity');
        $group->smallestAge=request('ageSmallestLimit');
        $group->largestAge=request('ageLargestLimit');
        $group->save();
       

       

        $organization=new \App\organization();
        $organization->organizationType=request('orgType');
        $organization->organizationName=request('orgName');
        $organization->organizationCountry=request('orgCountry');
        $organization->organizationCity=request('orgCity');
        $organization->organizationAddress=request('orgLoc');
        $organization->gid=$group->id;  //group id
        $organization->save();


        // save the data to database
        $limit1=request('ageSmallestLimit');
        $limit=request('ageLargestLimit');
      
        if($limit >= 18 && $limit1 <= 18)
        {
            return view('visitorPage.groupRequest3') -> with([
                'num' => request('howMany'),
                'gid' => $group->id
               
            ]);
        }
        else if($limit >= 18 && $limit1 >= 18)
        {
            
            return view('visitorPage.groupRequest3') -> with([
                'num' => $total,
                'gid' => $group->id
               
            ]);
        }
        else
        {
            $groupId=$group->id;
            return view('visitorPage.groupVisitationForm',compact('groupId'));
        }
            
        session()->flash('message','we will notify u soon,until then');

        return view('visitorPage.messagePage');
      
       
    }
    public function timeSet($id)
    {
        request()->validate([
        'setTime' => ['required']
        ]);
        $form=\App\request_form::findorFail($id);
        $form->approvedTime=request('setTime');
        $form->save();
        $visitor=\App\visitor::all();
        $staff=\App\User::findorFail(auth()->id());
        $req=\App\request_form::findorFail($rid);
        $vid=$req->vid;
        
        if($req->rep == "staff")
        {
            $rep=\App\User::where('id','=',$vid)->first();
        }
        elseif($req->rep == "visitor")
        {
            $rep=\App\visitor::where('id','=',$vid)->first();
        }
        
        $group=\App\group::where('vid','=',$vid)->first();
        
        $organization=\App\organization::where('rid','=',$group->id)->first();
        
        
        $limit=$group->largestAge;
        if($limit >= 18)
        {
            $grouprep=\App\group_rep::all()->where('groupRep','=',$vid);
            
        }
        return view('staffPage.staffGroupPage') ->with([
            'group' => $group,
            'groupRep' => $grouprep,
            'form' => $req,
            'visitor'=> $visitor,
            'staff'=> $staff,
            'rep'=> $rep,
            'approved' => $req->approved,
            'org' =>  $organization
            
        ]);

            
    }
}

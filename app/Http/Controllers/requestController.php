<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class requestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function seeRequest()
    {
        $user=\App\User::findorFail(auth()->id());


        $Request=\App\request_form::latest('updated_at')->get();
        $individualRequest=$Request->where('staffid','=',$user->staffId);

        $Request2=\App\training::latest('updated_at')->get();
        $trainingRequest=$Request2->where('department','=',$user->departments);

        $Request3=\App\intern::latest('updated_at')->get();
        $internRequest=$Request2->where('staffid','=',$user->staffId);

        $event=\App\event::latest('updated_at')->get();
        $eventRequest=$event->where('sid','=',$user->staffId);

        return view('staffPage.listRequest')->with([
            'individual' => $individualRequest,
            'training' => $trainingRequest,
            'intern' => $internRequest,
            'event' => $eventRequest
        ]);





    }
    public function identifyRequest()
    {
        
        if(request('choice') == 'Individual')
        {
            return redirect('/visitorRegister');
        }
        elseif(request('choice') == "Training")
        {
            return redirect('/training');

        }
        else if(request('choice') == "Intern")
        {
            return redirect('/forIntern');

        }
        elseif(request('choice')== 'group')
        {
            return redirect('/staffGroupRequest');

        }
        elseif(request('choice') == 'event')
        {
            return redirect('/forEvent');
        }

      
    }

}

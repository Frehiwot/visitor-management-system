<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class guardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function departmentComputerList()
    {
       
        $staff=request('staff');
        // create staff model
        // find a way to get all records which satisfies the condtions
        $unapproved=\App\request_form::all() ->where('staffid','=',$staff);
       
        $datee=$unapproved->where('approved','=',1);
        
        $individual=$datee->where('visitationDate','=',date('Y-m-d'));
        $individuals=$individual->where('type','=','individual');
        
       

        $user=\App\User::all()->where('staffId','=',$staff);
        
        $visitor=\App\visitor::all();

        // $message=\App\message::all()->where('date','=',date('Y-m-d'));
        $message=\App\message::latest('datee')->get();
        $messages=$message->where('sid','=',$staff);
        $message2=$messages->where('messageDate','=',date('Y-m-d'));
        $message3=$messages->where('messageType','=','availablity');
      
        
        
        

        // fetch full information and check if there is any visitor associated with theses staff memeber and display it to the guard
           
        return view('guardPage.Interface1Detail') -> with([
            'staff' => $user,
            'stafff' => $individuals,
            'visitor' => $visitor,
            'messages' => $message3
        ]);

    }
    public function newMessage()
    {
        // retrives messages posted today
        $message=\App\message::latest('datee')->get();
        $latestMessage=$message->where('messageDate','=',date('Y-m-d'));
        $staff=\App\User::all();
        return view('guardPage.newMessage')->with([
            'latestMessage' => $latestMessage,
            'staff' => $staff
        ]);

    }
   
    public function visitorDetail($id)
    {
        $visitor=\App\visitor::findorFail($id);
        // $download=DB::table('table_name')->get();
        // return $download->filename;
        $form=\App\request_form::where('vid','=',$id)->firstorFail();
        $notify=0;

        $staff=\App\User::where('staffId','=',$form->staffid)->firstorFail();

        $path='images/'.'AAU.JPG';
        
            return view('guardPage.visitorDetailPage')-> with([
                'form' => $form,
                'visitor' => $visitor,
                'path' => $path,
                'staff' => $staff,
                'approved' => $form->approved,
                'notify' => $notify
            ]);
    }

    
    public function sendNotification($id)
    {
        $visitor=\App\visitor::findorFail($id);
        // $download=DB::table('table_name')->get();
        // return $download->filename;
        $form=\App\request_form::where('vid','=',$id)->firstorFail();
        

        $staff=\App\User::where('staffId','=',$form->staffid)->firstorFail();
        
        

        // $user=\App\User::where('staffId','=',$departmentId->representative)->first();
            
        \App\User::find($staff->id)->notify(new \App\Notifications\visitorRequest($form->staffid,$form->id,'notify'));
        $notify=-1;
        $formn=\App\request_form::all()->where('visitationDate','=',date('Y-m-d'));
        $visitorr=\App\visitor::all();
        $send=new \App\sendNotification();
        $send->vid=$id;
        $send->save();

        $send=\App\sendNotification::all();
          return view('visitorPage.visitorsDetail') -> with([
                'form' => $formn,
                'visitor' => $visitorr,
                'notify' => $notify,
                'send' => $send

            ]);

            
           
    }
}

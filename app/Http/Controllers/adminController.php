<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class adminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function listUser()
    {
        $user=\App\User::all();
        return view('adminPage.listUser',compact('user'));
    }
    public function listDepartment()
    {
        $department=\App\Department::all();
        return view('adminPage.listDepartment',compact('department'));
    }
    public function listCollege()
    {
        $college=\App\College::all();
        return view('adminPage.listCollege',compact('college'));
    }
    public function edit($id)
    {

        $user=\App\User::findorFail($id);
        return view('adminPage.edit',compact('user'));
    }
    public function update($id)
    {
        request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'middlename' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'gender' => ['required'],
            'type' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => ['required',  'max:10'],
            'department' => ['required'],
            'college' => ['required'],
            'id' => ['required','min:10','max:11'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        
        $user=\App\User::findorFail($id);
        $user->firstname=request('name');
        $user->middlename=request('middlename');
        $user->lastname=request('lastname');
        $user->gender=request('gender');
        $user->department=request('department');
        $user->college=request('college');
        $user->staffId=request('id');
        $user->phone=request('phone');
        $user->email=request('email');
        
        $user->save();

        $user=\App\User::all();
        return view('adminPage.listUser',compact('user'));

        
    }
    public function destroy($id)
    {
        
        $user=\App\User::findorFail($id)->delete();
        return redirect('/listUser');
    }
    public function generateReport()
    {
        $user=\App\User::all()->where('type','=','staff');
        
        $form=\App\request_form::all()->where('type','=','individual');
        
        
        $formApproved=$form->where('approved','>=',1);
        $formDisapproved=$form->where('approved','=',0);
        $formDepartment=$form->where('whomToVisit','=','Department');
        $formDepartmentApproved=$formDepartment->where('approved','=',1);
        $formStaff=$form->where('whomToVisit','=','staff');
        
        $formStaffApproved=$formStaff->where('approved','=',1);
        $property=\App\property::all();
        $department=\App\department::all();
        $college=\App\college::all();
        $p=0;
        return view('adminPage.generateReport')->with([
            'user' => $user,
            'form' => $form,
            'formApproved' => $formApproved,
            'formDisapproved' => $formDisapproved,
            'formDepartment' => $formDepartment,
            'formStaff' => $formStaff,
            'property' => $property,
            'formStaffApproved' => $formStaffApproved,
            'formDepartmentApproved' => $formDepartmentApproved,
            'department' => $department,
            'college' => $college,
            'p' =>0
        ]);
    }
    public function registration()
    {
        return view('adminPage.depReg');
    }
    public function registration2()
    {
        return view('adminPage.colReg');
    }
    public function colRegistration()
    {
        $college=new \App\college();
        $college->name=request('name');
        $college->location=request('campus');
        $college->represenative=request('rep');
        $college->university=request('');
        $college->save();
    }
    public function showRegisterForm()
    {
        $department=\App\department::all();
        $user=\App\User::find(auth()->id());
        $dep=\App\department::where('name','=',$user->department)->first();
        $col=\App\department::where('name','=',$user->department)->first();
        return view('auth.register')->with([
            'department' => $department,
            'dep' => $dep,
            'user' => $user
        ]);
    }

    
}

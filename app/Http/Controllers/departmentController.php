<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class departmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function edit($id)
    {

        $department=\App\Department::findorFail($id);
        return view('adminPage.editDepartment',compact('department'));
    }
    public function update($id)
    {
        

        request()->validate([
            'depName' => ['required','min:3'],
            'rep' => ['required','min:10'],
            'location' => ['required'],
            'parent' => ['required'],
            'parentName' => ['required']
           ]);
  
           $department=\App\department::find($id);
           $department->name=request('depName');
           $staff=\App\User::where('staffId','=',request('rep'))->first();
           
           $department->representative=request('rep');
           if(request('parent') == "college")
           {
              $department->college=request('parentName');
           }
           $department->location=request('location');
           $department->save();
           if(!$staff)
           {
               session()->flash('message','u have updated succesfully.but u are trying to register unexisting rep,so please register the rep to make the data appropriate');
               return view('adminPage.messagePage');
           }
           $department=\App\department::all();
           return view('adminPage.listDepartment',compact('department'));
           

        
    }
    public function destroy($id)
    {
        
        $department=\App\department::findorFail($id)->delete();
        return redirect('/listDepartment');
    }
}

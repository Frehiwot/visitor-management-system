<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        session() ->flash('message','u have registerd succesfully');
        return view('home');
    }
    public function redirecting()
    {
        
        $user=\App\User::findorFail(auth()->id());
        $id=$user->staffId;
        
    //   if error exists check notification and request_forms table
        
        
        
        if($user->type === "guard")
        {
            $department=\App\department::all()->where('college','=',$user->college);
            
            $college=\App\college::where('name','=',$user->college)->first();
            $staff=\App\User::all()->where('type','=','staff');

            return view('guardPage.Interface1')->with([
                'department' => $department,
                'college' => $college,
                'staff' => $staff
            ]);
        }
        elseif($user->type === "staff")
        {
            $department=\App\department::where('name','=',$user->department)->first();
            if($department->representative == $user->staffId)
            {
                $notify=$user->unreadNotifications;
                return view('staffPage.staffInterface1')->with([
                    'rep' => 1,
                    'notify' => $notify
                ]);
            }
            else{
                $notify=$user->unreadNotifications;
                return view('staffPage.staffInterface1')->with([
                    'rep' => 0,
                    'notify' => $notify
                ]);
            }
           
        }
        else{
            $notify=$user->unreadNotifications;
            return view('adminPage.adminPage',compact('notify'));

        }
      
        
    }
    public function notification()
    {
        $user=\App\User::findorFail(auth()->id());
        $id=$user->staffId;
        
    //   if error exists check notification and request_forms table
        
        
        
        if($user->type === "guard")
        {
            $notify=$user->unreadNotifications;
            return view('guardPage.notificationPage',compact('notify'));
        }
        else
        {
            $notify=$user->unreadNotifications;
            return view('staffPage.staffInterface1',compact('notify'));
        }
    }
}

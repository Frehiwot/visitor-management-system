<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class visitorController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function index()
    {
        $id=auth()->id();
        if(!$id)
        {
            $attribute=request()->validate([
                'name' => ['required', 'string', 'min:3','max:255'],
                'middlename' => ['required', 'string', 'min:3','max:255'],
                'lastname' => ['required', 'string', 'min:3','max:255'],
                'gender' => ['required'],
                'country' => ['required','min:3'],
                'city' => ['required','min:3'],
                'Age' => ['required'],
                'property' => ['required'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['required', 'min:10','max:10'],
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                
            ]);
    
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
        }
        else{
            
            request()->validate([
                'name' => ['required', 'string', 'min:3','max:255'],
                'middlename' => ['required', 'string', 'min:3','max:255'],
                'lastname' => ['required', 'string', 'min:3','max:255'],
                'gender' => ['required'],
                'country' => ['required','min:3'],
                'city' => ['required','min:3'],
                'Age' => ['required'],
                'property' => ['required'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['required', 'min:10','max:10'],
               
                
            ]);
           
        }
        

         
            $visitor=new \App\visitor();
        
           
            $visitor->firstname = request('name');
            $visitor ->middlename = request('middlename');
            $visitor->lastname=request('lastname');
            $visitor->gender= request('gender');
            $visitor->email= request('email');
            $visitor->phone= request('phone');
            $visitor->country= request('country');
            $visitor->city= request('city');
            $visitor->age= request('Age') ;
            if(!$id){
                $type=request('vtype');
                $visitor->photo = $imageName;
            }
            elseif($id)
            {
               
                $type="individual";
            }

            $visitor->save();
            
        $property=request('property');
        $college=\App\college::all();
        $department=\App\department::all();
        $staff=\App\User::all();

        


        if($type === "individual")
        {
            $min=date('Y-m-d');
              return view('visitorPage.visitationForm') -> with([
                  'property' => $property,
                  'visitor' => $visitor->id,
                  'college' => $college,
                  'department' => $department,
                  'staff' => $staff,
                  'id' => $id,
                  'min' => $min

                  
              ]);
        }
        elseif($type === "group")
        {
            return view('visitorPage.groupRequest')->with([
                'vid' => $visitor->id,
                'property'=> $property,
                'id' => $id
            ]);
        }
        
    }

    public function visitationRequest()
    {
        if(!auth()->id())
        {
            $attribute=request()->validate([
                
                'college' => ['required'],
                'reasons' => ['required'],
                'datee' => ['required'],
                'reason' => ['required'],
                'staff' => ['required'],
                'department' => ['required'],
            ]);
            
            if(request('property') === "yes")
            {
                
                \App\property::create([
                    'propertyType' => request('propertyType'),
                    'propertyName' => request('propertyName'),
                    'quantity' => 1,
                    'identificationNumber' => request('propertyNumber'),
                    'vid' => request('visitorId')

                ]);
            }
            
            $college=request('college');
            
            $department=request('department');
            
            $staffId=request('staff');
            
            $staffUser=\App\User::find($staffId);
            
            $departmentId=\App\department::where('name','=',$department)->first();
            
            if($department !== "none")
            {
                
                if(($staffUser->department !== $department) || ($departmentId->college !== $college))
                {
                    return "invalid data";
                }
            }
            
            $reques=new \App\request_form();
            
            $visitor=\App\visitor::findorFail(request('visitorId'));
            $reques->vid=request('visitorId');
            $reques->college=request('college');
            $reques->whomToVisit=request('reasons');

            $reques->visitationDate=request('datee');
            //  correct the request date
            $reques->requestDate=date('Y-m-d');
            $reques->Reason=request('reason');
            $reques->departmentName=$department;
            $reques->type='individual';
            if(request('reasons') == "staff")
            {

                $reques->staffId=$staffUser->staffId;
            }
            elseif(request('reasons') == "Department")
            {
                $user=\App\User::where('staffId','=', $departmentId->representative)->first();
                // return request('vtype');
                $reques->staffid=$user->staffId;
            }
        
            

            $reques->save();

            // saving notification

            // \App\notification::create([
            // 'sid' => 'lec/3456/09',
            // 'notification' => 'u have visitor request',
            // 'read' => 0,
            // 'rid' => $reques->id
            // ]);

       
            if(request('reasons') == "staff")
            {
                \App\User::find($staffId)->notify(new \App\Notifications\visitorRequest($staffUser->staffId,$reques->id,'individual'));

            }
            elseif(request('reasons') == "Department")
            {
                $user=\App\User::where('staffId','=', $departmentId->representative)->first();
                \App\User::find($user->id)->notify(new \App\Notifications\visitorRequest($user->staffId,$reques->id,'individual'));

            }
            
            session()->flash('message','thank u for ur collaboration,we will notify u through ur email and phone,until then ..');
            return view('visitorPage.messagePage');
        }
        elseif(auth()->id())
        {
            $attribute=request()->validate([
                
                'college' => ['required'],
                
                'datee' => ['required'],
                'reason' => ['required'],
                'department' => ['required'],
            ]);
            
            if(request('property') === "yes")
            {
                
                \App\property::create([
                    'propertyType' => request('propertyType'),
                    'propertyName' => request('propertyName'),
                    'quantity' => 1,
                    'identificationNumber' => request('propertyNumber'),
                    'vid' => request('visitorId')

                ]);
            }
            
            $college=request('college');
            
            $department=request('department');
            $staff=\App\User::findorFail(auth()->id());
            $staffId=$staff->staffId;
            
            $staffUser=\App\User::find($staffId);
            
            $departmentId=\App\department::where('name','=',$department)->first();
            
            if($department !== "none")
            {
                
                if( ($departmentId->college !== $college))
                {
                    return "invalid data";
                }
            }
            
            $reques=new \App\request_form();
            
            $visitor=\App\visitor::findorFail(request('visitorId'));
            $reques->vid=request('visitorId');
            $reques->college=request('college');
            $reques->whomToVisit="staff";
            $reques->visitationDate=request('datee');
            //  correct the request date
            $reques->requestDate=date('Y-m-d');
            $reques->Reason=request('reason');
            $reques->departmentName=$department;
            $reques->type='individual';
            

                $reques->staffId=$staff->staffId;
           
        
            

            $reques->save();

            // saving notification

            // \App\notification::create([
            // 'sid' => 'lec/3456/09',
            // 'notification' => 'u have visitor request',
            // 'read' => 0,
            // 'rid' => $reques->id
            // ]);

       
          
            return redirect('/staffApproval/'.$reques->id);
        }
        
        // $user->notify(new visitorRequest());
        
      
       
    }
    public function staffApproval($id)
    {
        $form=\App\request_form::findorFail($id);
        $form->approved=1;
       

        $form->save();
        $visitor=\App\visitor::findorFail($form->vid);

         // mail to the visitor the appropriate informTION

         \Mail::to($visitor->email)->send(
            new \App\Mail\notifingVisitor($visitor,$form )
        );

        //notifying the guard about the approval
        $user=\App\User::where('type','=','guard')->first();

        $approve=new \App\approve();
        $approve->requestId=$form->id;
        $approve->approvedby=$form->staffid;
        $approve->approveDate=Carbon::now();    //make it current timestamp
        $approve->save();

        \App\User::find($user->id)->notify(new \App\Notifications\visitorRequest($user->staffId,$form->id,'individual'));

        $staff=\App\User::where('staffId','=',$form->staffid)->firstorFail();

        session()->flash('message','u have create ur  request succesfully');
        return view('visitorPage.messagePage');

    }
    public function groupRegistration()
    {
        
        request()->validate([
         'orgType' => ['required'],
         'orgName' => ['required','min:3','max:255'],
         'orgCountry' => ['required','min:3','max:255'],
         'orgCity' => ['required','min:3','max:255'],
         'ageSmallestLimit' => ['required'],
         'ageLargestLimit' => ['required'],
         'howMany' => ['required'],
         'quantity'=> ['required'],
         'quantityF'=> ['required'],
         'visitReason'=> ['required'],
         'orgLoc' => ['required'],
         
         
        ]);

        
        $vid=request('vid');
        
        $visitor=\App\visitor::findorFail($vid);
        $total=request('quantityF') + request('quantity');

        $group=new \App\group();
        $group->vid=$visitor->id;
        $group->numberOfFemale=request('quantityF');
        $group->numberOfMale=request('quantity');
        $group->smallestAge=request('ageSmallestLimit');
        $group->largestAge=request('ageLargestLimit');
        $group->save();
       

        if(request('property') == "yes")
        {
            
        //    find a way to validate property
             
            \App\property::create([
                'propertyType' => request('Type'),
                'propertyName' => request('propertyName'),
                'quantity' => request('quantity'),
                'identificationNumber' => request('propertyNumber'),
                'vid' => $group->id

            ]);
           
            
        }

        $organization=new \App\organization();
        $organization->organizationType=request('orgType');
        $organization->organizationName=request('orgName');
        $organization->organizationCountry=request('orgCountry');
        $organization->organizationCity=request('orgCity');
        $organization->organizationAddress=request('orgLoc');
        $organization->gid=$group->id;  //group id
        $organization->save();


        // save the data to database
        $limit=request('ageLargestLimit');
        $limit1=request('ageSmallestLimit');
      
        if($limit >= 18 && $limit1 <= 18)
        {
            return view('visitorPage.groupRequest3') -> with([
                'num' => request('howMany'),
                'gid' => $group->id
               
            ]);
        }
        else if($limit >= 18 && $limit1 >= 18)
        {
            
            return view('visitorPage.groupRequest3') -> with([
                'num' => $total,
                'gid' => $group->id
               
            ]);
        }
        else
        {
            $groupId=$group->id;
            return view('visitorPage.groupVisitationForm',compact('groupId'));
        }
            session()->flash('message','we will notify u soon,until then');

            return view('visitorPage.messagePage');
      
       
    }

   

  
    // 
    public function visitorDetail()
    {
        // retrive visitor detail and pass it to VisitorDetail view

        return view('visitorPage.visitorsDetail');
    }
    public function currentVisitorDetail()
    {
        //current date
        //change visitationDate into date and use date('Y-m-d)
        $form=\App\request_form::all()->where('visitationDate','=',date('Y-m-d'));
        $visitor=\App\visitor::all();
        $notify=0;
        $send=\App\sendNotification::all();
        return view('visitorPage.visitorsDetail') -> with([
            'form' => $form,
            'visitor' => $visitor,
            'notify' => $notify,
            'send' => $send
        ]);

        
        
    }

    public function groupFinalRegistration($gid)
    {
        
    //    u have to find a way to validate on this side
        

          

           $group=\App\group::findorFail($gid);
           if(!auth()->id())
           {
            $rep=\App\visitor::findorFail($group->vid);
            $grouprep=new \App\group_rep();
            $grouprep->groupId=$group->id;
            $grouprep->visitorId=$visitor->id;
            $grouprep->save();

           }
           else if(auth()->id())
           {
            $rep=\App\User::findorFail($group->vid);
            
           }
           
            $limit=request('limit');
            for($i=1;$i<=$limit;$i++)
            { 
                $visitor=new \App\visitor();
                $name='name'.$i;
                $middlename='middlename'.$i;
                $lastname='lastname'.$i;
                $gender='gender'.$i;
                $age='age'.$i;
                $phone='phone'.$i;
                $email='email'.$i;
                $image='image'.$i;
               
                $visitor->firstname=request($name);
                $visitor->middlename=request($middlename);
                $visitor->lastname=request($lastname);
                $visitor->gender=request($gender);
                $visitor->age=request($age);
                $visitor->phone=request($phone);
                $visitor->email=request($email);
                //country and city have to be null
                if(!auth()->id())
                {
                    $visitor->country=$rep->country;
                    $visitor->city=$rep->city;
                }
                elseif(auth()->id())
                {
                    $visitor->country="Ethiopia";
                    $visitor->city="Addis Ababa";
                }
                
                $visitor->photo=request($image);
                $visitor->save();
                // 
                $grouprep=new \App\group_rep();
                $grouprep->groupId=$group->id;
                $grouprep->visitorId=$visitor->id;
                $grouprep->save();
            }
            $groupId=$group->id;
            $college=\App\college::all();
            
            return view('visitorPage.groupVisitationForm')->with([
                'groupId' => $groupId,
                'college' => $college,
                'id' => auth()->id()
            ]);
        }
        public function groupvisitationFormSubmition($id)
        {
          // id is group id
          
            $group=\App\group::findorFail($id);
            
          $attribute=request()->validate([
            
            'college' => ['required'],
            
            'datee' => ['required'],
            'reason' => ['required']
            
            
            ]);
           

           
            $college=request('college');
            

            // merge staff and department form in to one

            // by assuming that i merge them in to one i will fill out the database
            
            $reques=new \App\request_form();
           
            
            $reques->vid=$group->vid;
            $reques->college=request('college');
            $reques->whomToVisit=request('college');
            $reques->visitationDate=request('datee');
            //  correct the request date
            $reques->requestDate=date('Y-m-d');
            
            $reques->Reason=request('reason');
            $reques->departmentName="none";
            //finding representative

            $collegeId=\App\college::where('name','=',$college)->first();
            
            $reques->staffId=$collegeId->representative;
           
            $reques->type="group";
            if(auth()->id())
            {
                $reques->rep="staff";
            }
            elseif(!auth()->id())
            {
                $reques->rep="visitor";
            }
           
            $reques->save();

            // saving notification

            // \App\notification::create([
            // 'sid' => $collegeId->represenative,
            // 'notification' => 'u have group visitor request request',
            // 'read' => 0,
            //  'type' => 'group',
            // 'rid' => $reques->id
            // ]);
            //
            $user=\App\User::where('staffId','=',$collegeId->representative)->first();
            
            \App\User::find($user->id)->notify(new \App\Notifications\visitorRequest($collegeId->representative,$reques->id,'group'));
            
            session()->flash('message','we will notify u soon,until then');

            return view('visitorPage.messagePage');
        }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class collegeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function edit($id)
    {

        $college=\App\college::findorFail($id);
        return view('adminPage.editCollege',compact('college'));
    }
    public function update($id)
    {
        

        request()->validate([
            'colName' => ['required','min:3'],
            'rep' => ['required','min:10'],
            'location' => ['required'],
            'parent' => ['required'],
            'parentName' => ['required']
           ]);
  
           $college=\App\college::find($id);
           $college->name=request('colName');
           $staff=\App\User::where('staffId','=',request('rep'))->first();
           
           $college->representative=request('rep');
           if(request('parent') == "university")
           {
              $college->college=request('parentName');
           }
           $college->location=request('location');
           $college->save();
           if(!$staff)
           {
               session()->flash('message','u have updated succesfully.but u are trying to register unexisting rep,so please register the rep to make the data appropriate');
               return view('adminPage.messagePage');
           }
           $college=\App\college::all();
           return view('adminPage.listCollege',compact('college'));
           

        
    }
    public function destroy($id)
    {
        
        $college=\App\college::findorFail($id)->delete();
        return redirect('/listCollege');
    }
    public function departmentCollege($id)
    {
        $college=\App\college::findorFail($id);
        return view('adminPage.collegeDepartment',compact('college'));
    }
    public function storeDepartment($id)
    {
        $college=\App\college::findorFail($id);
         request()->validate([
          'depName' => ['required','min:3'],
          'rep' => ['required','min:10'],
         
         ]);

         $department=new \App\department();
         $department->name=request('depName');
         $staff=\App\User::where('staffId','=',request('rep'))->first();
         
         $department->representative=request('rep');
         
        $department->college=$college->name;
        
         $department->location=$college->location;
         $department->save();
         
         if(!$staff)
         {
             session()->flash('message','u have registered succesfully.but u are trying to register unexisting rep,so please register the rep to make the data appropriate');
             
         }
         else{
            session()->flash('message','u have registered succesfully');
           
         }
         $college=\App\College::all();
        return view('adminPage.listCollege',compact('college'));

         
    }
    public function departmentCollegeList($id)
    {
        $college=\App\College::find($id);
        $department=\App\Department::all()->where('college','=',$college->name);
        return view('adminPage.listDepartment',compact('department'));
    }
}
